import { NextFunction, Request,Response } from "express";
import { IncomingHttpHeaders } from "http";
import Joi from "joi";
import { sep } from "path";
import { ifParamCheckConfig, IRouteBase, RequestEx } from "./define";



function parseErrorLine(err: Error) {
    if (!err || !(err instanceof Error) || !err.stack) {
        return '[-]'
    }

    const stack = err.stack;
    const stackArr = stack.split('\n');
    let callerLogIndex = (stackArr.length > 0) ? 1 : 0;
    // for (let i = 0; i < stackArr.length; i++) {
    //     if (stackArr[i].indexOf('makeError') > 0 && i + 1 < stackArr.length) {
    //         callerLogIndex = i + 1;
    //         break;
    //     }
    // }

    if (callerLogIndex !== 0) {
        const callerStackLine = stackArr[callerLogIndex] as string;
        let list = callerStackLine.split(/\(|\)/)
        if (list[1] && list[1].length > 0) {
            return `[${list[1].replace(process.cwd(), '').replace(/\\/g, "/").replace('/', '')}]`
        }
        else {
            return `[${callerStackLine.substring(callerStackLine.lastIndexOf(sep) + 1, callerStackLine.lastIndexOf(':'))}]`;
        }
    } else {
        return '[-]';
    }
}

export class HandleUnit {
    symbol: symbol
    constructor(symbol?: symbol) {
        this.symbol = symbol || Symbol()
    }

    routeMap: {
        [f: string]: IRouteBase & {
            value: (param: { [x: string]: any }, method: string, headers: IncomingHttpHeaders, req?: RequestEx) => Promise<any>;
        }
    } = {};
    beforeList: (IRouteBase & {
        value: (req: Request, res: Response) => any
    })[] = [];
    afterList: (IRouteBase & {
        value: (req: Request, res: Response) => any
    })[] = [];


    normalCheck(checkMap: ifParamCheckConfig, params: { [x: string]: any }) {
        for (let key in checkMap) {
            let cfg = checkMap[key];
            if (cfg.required && !params.hasOwnProperty(key)) {
                // 这里没找到 抛出一个异常
                throw Error(`param [${key} required and type limit ${cfg.type}]`)
            }
            else if (!params.hasOwnProperty(key)) {
                continue;
            }

            if (cfg.type == typeof params[key]) {
                continue
            }

            // 这里类型不同
            if (!cfg.change) {
                throw Error(`param [${key} type limit ${cfg.type}]`)
            }
            else {
                switch (cfg.type) {
                    case "bigint": params[key] = BigInt(params[key]); break;
                    case "boolean": {
                        if (params[key] == true || params[key] == "true") {
                            params[key] = true;
                        }
                        else {
                            params[key] = false;
                        }
                        // params[key] = Boolean(params[key]);
                        break;
                    }
                    case "function": throw Error(`param [${key} type limit ${cfg.type}]`);
                    case "number": params[key] = Number(params[key]);
                        // 这里增加一个小优化 ，如果是整数那么直接转换成整数
                        if (params[key] = parseInt(params[key])) {
                            params[key] = parseInt(params[key])
                        }
                        break;
                    case "object": params[key] = JSON.parse(params[key].toString()); break;
                    case "string": params[key] = String(params[key]); break;
                    case "symbol": params[key] = Symbol(params[key]); break;
                    default: break;
                }
            }
        }

        return params;
    }

    paramsCheck(checkMap: IRouteBase, params: { [x: string]: any }) {
        if (checkMap.params == undefined) return params;
        for (let i = 0; i < checkMap.params.length; i++) {
            let v = checkMap.params[i];
            switch (v.type) {
                case "class-validator":
                    break;
                case "joi":
                    // 
                    let err = (v.infos as Joi.ObjectSchema).validate(params, {
                        allowUnknown: true,
                        stripUnknown: true
                    })
                    if (err.error) {
                        throw { code: -1, errMsg: err.error.message, stack: err.error.stack }
                    }
                    params = err.value;
                    break;
                default:
                    params = this.normalCheck(v.infos, params);
                    break;
            }
        }

        return params;
    }

    // 调用接口
    getRouteFunc(Name: string, req: RequestEx) {
        let routeObj = this.routeMap[Name];
        if (!routeObj) {
            return undefined;
        }
        req.params = this.paramsCheck(routeObj, req.params)
        return routeObj.value.bind(routeObj.target, req.params, req.method.toUpperCase(), req.headers, req)
    }

    /**
     * 调用模块
     * @param pathlist 
     * @param req 
     * @param res 
     * @param next 
     */
    async route(Name: string, req: RequestEx, res: Response, next: NextFunction) {
        try {
            let routeFunc = this.getRouteFunc(Name, req)
            if (!routeFunc) {
                req.responseData = {
                    code: -1,
                    errMsg: "can not find [" + req.path + "]"
                }
                return true;
            }

            // 这里开始调用响应
            req.responseData = await routeFunc()
            if (req.responseData == undefined) {
                req.responseData = { code: 0 }
            }
        }
        catch (e) {
            if (!e) {
                req.responseData = {
                    code: -1,
                    errMsg: "error bug no message"
                }
            }
            else if (typeof e == 'string') {
                req.responseData = e
            }
            else if (e instanceof Error) {
                req.responseData = {
                    code: -1,
                    errMsg: e.message,
                    __line__: parseErrorLine(e)
                }
            }
            else {
                req.responseData = e;
            }
        }

        return true
    }
    /**
    * 调用模块
    * @param pathlist 
    * @param req 
    * @param res 
    * @param next 
    */
    async before(req: RequestEx, res: Response) {
        for (let i = 0; i < this.beforeList.length; i++) {
            let routeMap = this.beforeList[i];
            try {
                req.params = this.paramsCheck(routeMap, req.params);
                await routeMap.value.apply(routeMap.target, [req, res]);
            }
            catch (e) {
                // 出钱异常的化
                if (!e) {
                    req.responseData = {
                        code: -1,
                        errMsg: "error bug no message"
                    }
                }
                else if (typeof e == 'string') {
                    req.responseData = e
                }
                else if (e instanceof Error) {
                    req.responseData = {
                        code: -1,
                        errMsg: e.message,
                        __line__: parseErrorLine(e)
                    }
                }
                else {
                    req.responseData = e;
                }
                return false
            }
        }
        return true
    }
    /**
    * 调用模块
    * @param pathlist 
    * @param req 
    * @param res 
    * @param next 
    */
    async after(req: RequestEx, res: Response) {
        for (let i = 0; i < this.afterList.length; i++) {
            let routeMap = this.afterList[i];
            try {
                req.params = this.paramsCheck(routeMap, req.params);
                await routeMap.value.apply(routeMap.target, [req, res]);
            }
            catch (e) {
                // 出钱异常的化
                if (!e) {
                    req.responseData = {
                        code: -1,
                        errMsg: "error bug no message"
                    }
                }
                else if (typeof e == 'string') {
                    req.responseData = e
                }
                else if (e instanceof Error) {
                    req.responseData = {
                        code: -1,
                        errMsg: e.message,
                        __line__: parseErrorLine(e)
                    }
                }
                else {
                    req.responseData = e;
                }
                return false
            }
        }
        return true
    }
}
