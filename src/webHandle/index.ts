import Joi from "joi"
export { SeParamConfigType } from "./define"
import { SeParamConfigType } from "./define"
import { HandleMap } from "./HandleMap"
export * from "./define"

export class WebHandle {
    mainPath = ""
    init(mainPath: string, env?: (() => string) | string) {
        this.mainPath = mainPath.replace(/\\/g, "/")
        this.setEnv(env || "")

        // 合并一下啊各种模块
        this.routeMap.mergeModule(this.mainPath)
    }

    private globalMap = new HandleMap("global")
    private envMap = new HandleMap("env")
    private routeMap = new HandleMap("route")

    /**
    * 设置模块的归属
    */
    class(prePath?: string | NodeModule): ClassDecorator {
        return this.routeMap.class(prePath)
    }

    /**
     * 注册路由
     */
    route(Name?: string): MethodDecorator {
        return this.routeMap.route(Name);
    }

    /**
     * 注册参数检查
     */
    params(Name: string, type: SeParamConfigType, required: boolean, change: boolean): MethodDecorator {
        return this.routeMap.params(Name, type, required, change);
    }

    /**
     * 注册参数检查 支持joi模式
     */
    paramJoi(objSch: Joi.ObjectSchema<any>): MethodDecorator {
        return this.routeMap.paramJoi(objSch);
    }

    /**
     * 模块方法调用前的调用，主要是给数据处理提供前置准备
     */
    before(): MethodDecorator {
        return this.routeMap.before();
    }

    /**
     * 模块方法调用后调用，主要是给数据处理提供最后的操作
     */
    after(): MethodDecorator {
        return this.routeMap.after();
    }

    /**
     * 获取一个响应模块
     * @param prePath 可以设置相对路径字符串，或者模块对象
     */
    getModule(prePath: string | NodeModule) {
        if (typeof prePath != "string") {
            prePath = prePath.filename;
        }

        return this.routeMap.getModule(prePath)
    }

    getEnvModule() {
        return this.envMap.getModule(this.getEnv())
    }

    getGlobalModule() {
        return this.globalMap.getModule("")
    }

    // 当前的环境
    private _env: (() => string) | string = ""
    /**
     * 设置当前环境 （主要是提供给处理方法调用环境判断）
     * @param env 环境参数
     */
    setEnv(env: (() => string) | string) {
        this._env = env
    }

    /**
     * 获取当前环境
     */
    getEnv() {
        if (typeof this._env == "function") {
            return this._env()
        }
        else {
            return this._env;
        }
    }

    /**
     * 设置模块的归属
     */
    envClass(prePath?: string | NodeModule): ClassDecorator {
        return this.envMap.class(prePath)
    }

    /**
     * 模块方法调用前的调用，主要是给数据处理提供前置准备
     */
    envBefore(): MethodDecorator {
        return this.envMap.before();
    }

    /**
     * 模块方法调用后调用，主要是给数据处理提供最后的操作
     */
    envAfter(): MethodDecorator {
        return this.envMap.after();
    }

    /**
     * 全局模块
     */
    globalClass(): ClassDecorator {
        return this.globalMap.class("")
    }

    /**
     * 模块方法调用前的调用，主要是给数据处理提供前置准备
     */
    globalBefore(): MethodDecorator {
        return this.globalMap.before();
    }

    /**
     * 模块方法调用后调用，主要是给数据处理提供最后的操作
     */
    globalAfter(): MethodDecorator {
        return this.globalMap.after();
    }
}