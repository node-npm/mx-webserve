"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebHandle = void 0;
const HandleMap_1 = require("./HandleMap");
__exportStar(require("./define"), exports);
class WebHandle {
    constructor() {
        this.mainPath = "";
        this.globalMap = new HandleMap_1.HandleMap("global");
        this.envMap = new HandleMap_1.HandleMap("env");
        this.routeMap = new HandleMap_1.HandleMap("route");
        // 当前的环境
        this._env = "";
    }
    init(mainPath, env) {
        this.mainPath = mainPath.replace(/\\/g, "/");
        this.setEnv(env || "");
        // 合并一下啊各种模块
        this.routeMap.mergeModule(this.mainPath);
    }
    /**
    * 设置模块的归属
    */
    class(prePath) {
        return this.routeMap.class(prePath);
    }
    /**
     * 注册路由
     */
    route(Name) {
        return this.routeMap.route(Name);
    }
    /**
     * 注册参数检查
     */
    params(Name, type, required, change) {
        return this.routeMap.params(Name, type, required, change);
    }
    /**
     * 注册参数检查 支持joi模式
     */
    paramJoi(objSch) {
        return this.routeMap.paramJoi(objSch);
    }
    /**
     * 模块方法调用前的调用，主要是给数据处理提供前置准备
     */
    before() {
        return this.routeMap.before();
    }
    /**
     * 模块方法调用后调用，主要是给数据处理提供最后的操作
     */
    after() {
        return this.routeMap.after();
    }
    /**
     * 获取一个响应模块
     * @param prePath 可以设置相对路径字符串，或者模块对象
     */
    getModule(prePath) {
        if (typeof prePath != "string") {
            prePath = prePath.filename;
        }
        return this.routeMap.getModule(prePath);
    }
    getEnvModule() {
        return this.envMap.getModule(this.getEnv());
    }
    getGlobalModule() {
        return this.globalMap.getModule("");
    }
    /**
     * 设置当前环境 （主要是提供给处理方法调用环境判断）
     * @param env 环境参数
     */
    setEnv(env) {
        this._env = env;
    }
    /**
     * 获取当前环境
     */
    getEnv() {
        if (typeof this._env == "function") {
            return this._env();
        }
        else {
            return this._env;
        }
    }
    /**
     * 设置模块的归属
     */
    envClass(prePath) {
        return this.envMap.class(prePath);
    }
    /**
     * 模块方法调用前的调用，主要是给数据处理提供前置准备
     */
    envBefore() {
        return this.envMap.before();
    }
    /**
     * 模块方法调用后调用，主要是给数据处理提供最后的操作
     */
    envAfter() {
        return this.envMap.after();
    }
    /**
     * 全局模块
     */
    globalClass() {
        return this.globalMap.class("");
    }
    /**
     * 模块方法调用前的调用，主要是给数据处理提供前置准备
     */
    globalBefore() {
        return this.globalMap.before();
    }
    /**
     * 模块方法调用后调用，主要是给数据处理提供最后的操作
     */
    globalAfter() {
        return this.globalMap.after();
    }
}
exports.WebHandle = WebHandle;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBR0EsMkNBQXVDO0FBQ3ZDLDJDQUF3QjtBQUV4QixNQUFhLFNBQVM7SUFBdEI7UUFDSSxhQUFRLEdBQUcsRUFBRSxDQUFBO1FBU0wsY0FBUyxHQUFHLElBQUkscUJBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUNuQyxXQUFNLEdBQUcsSUFBSSxxQkFBUyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzdCLGFBQVEsR0FBRyxJQUFJLHFCQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7UUFnRXpDLFFBQVE7UUFDQSxTQUFJLEdBQTRCLEVBQUUsQ0FBQTtJQThEOUMsQ0FBQztJQXpJRyxJQUFJLENBQUMsUUFBZ0IsRUFBRSxHQUE2QjtRQUNoRCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFBO1FBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLEVBQUUsQ0FBQyxDQUFBO1FBRXRCLFlBQVk7UUFDWixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7SUFDNUMsQ0FBQztJQU1EOztNQUVFO0lBQ0YsS0FBSyxDQUFDLE9BQTZCO1FBQy9CLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUE7SUFDdkMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsS0FBSyxDQUFDLElBQWE7UUFDZixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRDs7T0FFRztJQUNILE1BQU0sQ0FBQyxJQUFZLEVBQUUsSUFBdUIsRUFBRSxRQUFpQixFQUFFLE1BQWU7UUFDNUUsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxRQUFRLENBQUMsTUFBNkI7UUFDbEMsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxNQUFNO1FBQ0YsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2xDLENBQUM7SUFFRDs7T0FFRztJQUNILEtBQUs7UUFDRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUVEOzs7T0FHRztJQUNILFNBQVMsQ0FBQyxPQUE0QjtRQUNsQyxJQUFJLE9BQU8sT0FBTyxJQUFJLFFBQVEsRUFBRTtZQUM1QixPQUFPLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQztTQUM5QjtRQUVELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7SUFDM0MsQ0FBQztJQUVELFlBQVk7UUFDUixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFBO0lBQy9DLENBQUM7SUFFRCxlQUFlO1FBQ1gsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQTtJQUN2QyxDQUFDO0lBSUQ7OztPQUdHO0lBQ0gsTUFBTSxDQUFDLEdBQTRCO1FBQy9CLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFBO0lBQ25CLENBQUM7SUFFRDs7T0FFRztJQUNILE1BQU07UUFDRixJQUFJLE9BQU8sSUFBSSxDQUFDLElBQUksSUFBSSxVQUFVLEVBQUU7WUFDaEMsT0FBTyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7U0FDckI7YUFDSTtZQUNELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztTQUNwQjtJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNILFFBQVEsQ0FBQyxPQUE2QjtRQUNsQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0lBQ3JDLENBQUM7SUFFRDs7T0FFRztJQUNILFNBQVM7UUFDTCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsUUFBUTtRQUNKLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxXQUFXO1FBQ1AsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQTtJQUNuQyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxZQUFZO1FBQ1IsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ25DLENBQUM7SUFFRDs7T0FFRztJQUNILFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEMsQ0FBQztDQUNKO0FBM0lELDhCQTJJQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBKb2kgZnJvbSBcImpvaVwiXG5leHBvcnQgeyBTZVBhcmFtQ29uZmlnVHlwZSB9IGZyb20gXCIuL2RlZmluZVwiXG5pbXBvcnQgeyBTZVBhcmFtQ29uZmlnVHlwZSB9IGZyb20gXCIuL2RlZmluZVwiXG5pbXBvcnQgeyBIYW5kbGVNYXAgfSBmcm9tIFwiLi9IYW5kbGVNYXBcIlxuZXhwb3J0ICogZnJvbSBcIi4vZGVmaW5lXCJcblxuZXhwb3J0IGNsYXNzIFdlYkhhbmRsZSB7XG4gICAgbWFpblBhdGggPSBcIlwiXG4gICAgaW5pdChtYWluUGF0aDogc3RyaW5nLCBlbnY/OiAoKCkgPT4gc3RyaW5nKSB8IHN0cmluZykge1xuICAgICAgICB0aGlzLm1haW5QYXRoID0gbWFpblBhdGgucmVwbGFjZSgvXFxcXC9nLCBcIi9cIilcbiAgICAgICAgdGhpcy5zZXRFbnYoZW52IHx8IFwiXCIpXG5cbiAgICAgICAgLy8g5ZCI5bm25LiA5LiL5ZWK5ZCE56eN5qih5Z2XXG4gICAgICAgIHRoaXMucm91dGVNYXAubWVyZ2VNb2R1bGUodGhpcy5tYWluUGF0aClcbiAgICB9XG5cbiAgICBwcml2YXRlIGdsb2JhbE1hcCA9IG5ldyBIYW5kbGVNYXAoXCJnbG9iYWxcIilcbiAgICBwcml2YXRlIGVudk1hcCA9IG5ldyBIYW5kbGVNYXAoXCJlbnZcIilcbiAgICBwcml2YXRlIHJvdXRlTWFwID0gbmV3IEhhbmRsZU1hcChcInJvdXRlXCIpXG5cbiAgICAvKipcbiAgICAqIOiuvue9ruaooeWdl+eahOW9kuWxnlxuICAgICovXG4gICAgY2xhc3MocHJlUGF0aD86IHN0cmluZyB8IE5vZGVNb2R1bGUpOiBDbGFzc0RlY29yYXRvciB7XG4gICAgICAgIHJldHVybiB0aGlzLnJvdXRlTWFwLmNsYXNzKHByZVBhdGgpXG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5rOo5YaM6Lev55SxXG4gICAgICovXG4gICAgcm91dGUoTmFtZT86IHN0cmluZyk6IE1ldGhvZERlY29yYXRvciB7XG4gICAgICAgIHJldHVybiB0aGlzLnJvdXRlTWFwLnJvdXRlKE5hbWUpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOazqOWGjOWPguaVsOajgOafpVxuICAgICAqL1xuICAgIHBhcmFtcyhOYW1lOiBzdHJpbmcsIHR5cGU6IFNlUGFyYW1Db25maWdUeXBlLCByZXF1aXJlZDogYm9vbGVhbiwgY2hhbmdlOiBib29sZWFuKTogTWV0aG9kRGVjb3JhdG9yIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucm91dGVNYXAucGFyYW1zKE5hbWUsIHR5cGUsIHJlcXVpcmVkLCBjaGFuZ2UpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOazqOWGjOWPguaVsOajgOafpSDmlK/mjIFqb2nmqKHlvI9cbiAgICAgKi9cbiAgICBwYXJhbUpvaShvYmpTY2g6IEpvaS5PYmplY3RTY2hlbWE8YW55Pik6IE1ldGhvZERlY29yYXRvciB7XG4gICAgICAgIHJldHVybiB0aGlzLnJvdXRlTWFwLnBhcmFtSm9pKG9ialNjaCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5qih5Z2X5pa55rOV6LCD55So5YmN55qE6LCD55So77yM5Li76KaB5piv57uZ5pWw5o2u5aSE55CG5o+Q5L6b5YmN572u5YeG5aSHXG4gICAgICovXG4gICAgYmVmb3JlKCk6IE1ldGhvZERlY29yYXRvciB7XG4gICAgICAgIHJldHVybiB0aGlzLnJvdXRlTWFwLmJlZm9yZSgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaooeWdl+aWueazleiwg+eUqOWQjuiwg+eUqO+8jOS4u+imgeaYr+e7meaVsOaNruWkhOeQhuaPkOS+m+acgOWQjueahOaTjeS9nFxuICAgICAqL1xuICAgIGFmdGVyKCk6IE1ldGhvZERlY29yYXRvciB7XG4gICAgICAgIHJldHVybiB0aGlzLnJvdXRlTWFwLmFmdGVyKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog6I635Y+W5LiA5Liq5ZON5bqU5qih5Z2XXG4gICAgICogQHBhcmFtIHByZVBhdGgg5Y+v5Lul6K6+572u55u45a+56Lev5b6E5a2X56ym5Liy77yM5oiW6ICF5qih5Z2X5a+56LGhXG4gICAgICovXG4gICAgZ2V0TW9kdWxlKHByZVBhdGg6IHN0cmluZyB8IE5vZGVNb2R1bGUpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBwcmVQYXRoICE9IFwic3RyaW5nXCIpIHtcbiAgICAgICAgICAgIHByZVBhdGggPSBwcmVQYXRoLmZpbGVuYW1lO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMucm91dGVNYXAuZ2V0TW9kdWxlKHByZVBhdGgpXG4gICAgfVxuXG4gICAgZ2V0RW52TW9kdWxlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5lbnZNYXAuZ2V0TW9kdWxlKHRoaXMuZ2V0RW52KCkpXG4gICAgfVxuXG4gICAgZ2V0R2xvYmFsTW9kdWxlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5nbG9iYWxNYXAuZ2V0TW9kdWxlKFwiXCIpXG4gICAgfVxuXG4gICAgLy8g5b2T5YmN55qE546v5aKDXG4gICAgcHJpdmF0ZSBfZW52OiAoKCkgPT4gc3RyaW5nKSB8IHN0cmluZyA9IFwiXCJcbiAgICAvKipcbiAgICAgKiDorr7nva7lvZPliY3njq/looMg77yI5Li76KaB5piv5o+Q5L6b57uZ5aSE55CG5pa55rOV6LCD55So546v5aKD5Yik5pat77yJXG4gICAgICogQHBhcmFtIGVudiDnjq/looPlj4LmlbBcbiAgICAgKi9cbiAgICBzZXRFbnYoZW52OiAoKCkgPT4gc3RyaW5nKSB8IHN0cmluZykge1xuICAgICAgICB0aGlzLl9lbnYgPSBlbnZcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDojrflj5blvZPliY3njq/looNcbiAgICAgKi9cbiAgICBnZXRFbnYoKSB7XG4gICAgICAgIGlmICh0eXBlb2YgdGhpcy5fZW52ID09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2VudigpXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fZW52O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog6K6+572u5qih5Z2X55qE5b2S5bGeXG4gICAgICovXG4gICAgZW52Q2xhc3MocHJlUGF0aD86IHN0cmluZyB8IE5vZGVNb2R1bGUpOiBDbGFzc0RlY29yYXRvciB7XG4gICAgICAgIHJldHVybiB0aGlzLmVudk1hcC5jbGFzcyhwcmVQYXRoKVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaooeWdl+aWueazleiwg+eUqOWJjeeahOiwg+eUqO+8jOS4u+imgeaYr+e7meaVsOaNruWkhOeQhuaPkOS+m+WJjee9ruWHhuWkh1xuICAgICAqL1xuICAgIGVudkJlZm9yZSgpOiBNZXRob2REZWNvcmF0b3Ige1xuICAgICAgICByZXR1cm4gdGhpcy5lbnZNYXAuYmVmb3JlKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5qih5Z2X5pa55rOV6LCD55So5ZCO6LCD55So77yM5Li76KaB5piv57uZ5pWw5o2u5aSE55CG5o+Q5L6b5pyA5ZCO55qE5pON5L2cXG4gICAgICovXG4gICAgZW52QWZ0ZXIoKTogTWV0aG9kRGVjb3JhdG9yIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZW52TWFwLmFmdGVyKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5YWo5bGA5qih5Z2XXG4gICAgICovXG4gICAgZ2xvYmFsQ2xhc3MoKTogQ2xhc3NEZWNvcmF0b3Ige1xuICAgICAgICByZXR1cm4gdGhpcy5nbG9iYWxNYXAuY2xhc3MoXCJcIilcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmqKHlnZfmlrnms5XosIPnlKjliY3nmoTosIPnlKjvvIzkuLvopoHmmK/nu5nmlbDmja7lpITnkIbmj5DkvpvliY3nva7lh4blpIdcbiAgICAgKi9cbiAgICBnbG9iYWxCZWZvcmUoKTogTWV0aG9kRGVjb3JhdG9yIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2xvYmFsTWFwLmJlZm9yZSgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaooeWdl+aWueazleiwg+eUqOWQjuiwg+eUqO+8jOS4u+imgeaYr+e7meaVsOaNruWkhOeQhuaPkOS+m+acgOWQjueahOaTjeS9nFxuICAgICAqL1xuICAgIGdsb2JhbEFmdGVyKCk6IE1ldGhvZERlY29yYXRvciB7XG4gICAgICAgIHJldHVybiB0aGlzLmdsb2JhbE1hcC5hZnRlcigpO1xuICAgIH1cbn0iXX0=