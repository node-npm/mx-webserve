import Joi from "joi";
import { join, parse } from "path";
import { IRouteBase, SeParamConfigType, TypedPropertyDescriptorEx } from "./define";
import { HandleUnit } from "./HandleUnit";

function Obj2List(target: any, value: any, params: any) {
    let obj: IRouteBase & { value: any } = {
        target: target,
        params: [],
        value: value
    }

    if (params?.normal) {
        obj.params.push({
            type: "normal",
            infos: params?.normal
        })
    }

    if (params?.joi) {
        obj.params.push({
            type: "joi",
            infos: params.joi
        })
    }

    return obj
}
export class HandleMap {
    private routeMap: { [f: string]: HandleUnit } = {};
    private currNode: HandleUnit | undefined;
    private type: string;
    private mainPath: string | undefined;
    constructor(type: "route" | "env" | "global") {
        this.type = type;
    }

    mergeModule(fileRoute: string) {
        // this.routeMap[prePath].routeMap = Object.assign(this.routeMap[prePath].routeMap, this.currNode.routeMap)
        // if (this.currNode.afterList) this.routeMap[prePath].afterList.push(...this.currNode.afterList)
        // if (this.currNode.beforeList) this.routeMap[prePath].beforeList.push(...this.currNode.beforeList)

        for (let key in this.routeMap) {
            let file = key;
            let mmap = this.routeMap[key]
            // 判断一下是全路径的还是相对地址的
            if (file.indexOf(fileRoute) < 0) continue
            // 这里改成相对地址的模块
            let nKey = key.replace(fileRoute, "/");
            let n = this.getModule(nKey)
            n.routeMap = Object.assign(mmap.routeMap, n.routeMap)
            if (mmap.afterList) n.afterList.push(...mmap.afterList)
            if (mmap.beforeList) n.beforeList.push(...mmap.beforeList)
            delete this.routeMap[key]
        }
        this.mainPath = fileRoute;
    }

    hasModule(prePath: string) {
        if (this.routeMap[prePath]) true;
        return false;
    }

    private formateRoute(prePath: string) {
        prePath = prePath.replace(/\\/g, "/")
        if (this.mainPath) {
            prePath = prePath.replace(this.mainPath, "/")
        }

        prePath = prePath.replace(/\/\//g, "/")
        if (prePath == undefined || prePath == null || prePath == "") prePath = "/";
        if (prePath[0] != "/") prePath = "/" + prePath
        // if (prePath.charAt(0) != "/") prePath = "/" + prePath;

        return prePath;
    }

    getModule(prePath: string) {
        prePath = this.formateRoute(prePath)

        if (!this.routeMap[prePath]) this.routeMap[prePath] = new HandleUnit()
        return this.routeMap[prePath]
    }

    private _chechMethod(target: any) {
        if (!this.currNode) this.currNode = new HandleUnit()
        if (!target.___symbol) target.___symbol = this.currNode.symbol;
        if (target.___symbol != this.currNode.symbol) {
            // 表示注册的不是当前class的模块了，需要替换
            this.currNode = new HandleUnit
        }
    }

    /**
     * 设置模块的归属
     */
    class(prePath?: string | NodeModule): ClassDecorator {
        return (target) => {
            if (prePath == undefined) prePath = target.name
            if (typeof prePath != "string") {
                let pd = parse(prePath.filename);
                if (pd.name == "index") {
                    prePath = pd.dir;
                }
                else {
                    prePath = join(pd.dir, pd.name)
                }
            }

            prePath = this.formateRoute(prePath);

            if (!this.currNode) return;
            // 这里负责把当前注册的节点添加到目标节点库中
            if (this.routeMap[prePath]) {
                // 这里需要合并
                this.routeMap[prePath].routeMap = Object.assign(this.routeMap[prePath].routeMap, this.currNode.routeMap)
                if (this.currNode.afterList) this.routeMap[prePath].afterList.push(...this.currNode.afterList)
                if (this.currNode.beforeList) this.routeMap[prePath].beforeList.push(...this.currNode.beforeList)
                this.currNode = undefined;
            }
            else {
                this.routeMap[prePath] = this.currNode;
                this.currNode = undefined
            }
        }
    }

    methodPrepare(descriptor: TypedPropertyDescriptorEx<any>) {
        if (!descriptor.params) descriptor.params = { normal: {} };
    }

    /**
    * 注册路由
    */
    route(Name?: string): MethodDecorator {
        return (target, name, descriptor) => {
            this._chechMethod(target)
            this.methodPrepare(descriptor)
            if (Name == undefined && typeof name == "string") Name = name;
            if (Name == undefined || descriptor.value == undefined) return;
            if (this.currNode) {
                this.currNode.routeMap[Name] = Obj2List(target, descriptor.value, (descriptor as any).params)
            }
        }
    }

    /**
    * 检查路由参数 如果调用了那么就给这个函数增加一个参数检查的标识，否则就不处理
    */
    params(pName: string, pType: SeParamConfigType, required: boolean, change: boolean): MethodDecorator {
        return (target, name, descriptor: TypedPropertyDescriptorEx<any>) => {
            this.methodPrepare(descriptor)
            this._chechMethod(target)
            if (!this.currNode || !descriptor.params) {
                // 理论上不会进入这里的
                return;
            }
            if (typeof name == "string") {
                descriptor.params.normal[pName] = { type: pType, required: required, change: change }
            }
            else {
                // 这里如果传入的是 symbol 那么就比较难受了
                console.warn(`params check error on func ${name.toString()} ${pName} ${pType}`)
            }
        }
    }

    /**
   * 检查路由参数 如果调用了那么就给这个函数增加一个参数检查的标识，否则就不处理
   */
    paramJoi(objSch: Joi.ObjectSchema): MethodDecorator {
        return (target, name, descriptor: TypedPropertyDescriptorEx<any>) => {
            this.methodPrepare(descriptor)
            this._chechMethod(target)
            if (!this.currNode || !descriptor.params) {
                // 理论上不会进入这里的
                return;
            }
            descriptor.params.joi = objSch
        }
    }

    /**
     * 模块方法调用前的调用，主要是给数据处理提供前置准备
     */
    before(): MethodDecorator {
        return (target, name, descriptor: TypedPropertyDescriptorEx<any>) => {
            this.methodPrepare(descriptor)
            this._chechMethod(target)
            if (descriptor.value && this.currNode) {
                this.currNode.beforeList.push(Obj2List(target, descriptor.value, descriptor.params))
            }
        }
    }

    /**
     * 模块方法调用后调用，主要是给数据处理提供最后的操作
     */
    after(): MethodDecorator {
        return (target, name, descriptor: TypedPropertyDescriptorEx<any>) => {
            this.methodPrepare(descriptor)
            this._chechMethod(target)
            if (descriptor.value && this.currNode) {
                this.currNode.afterList.push(Obj2List(target, descriptor.value, descriptor.params))
            }
        }
    }
}
