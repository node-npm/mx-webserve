"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HandleUnit = void 0;
const path_1 = require("path");
function parseErrorLine(err) {
    if (!err || !(err instanceof Error) || !err.stack) {
        return '[-]';
    }
    const stack = err.stack;
    const stackArr = stack.split('\n');
    let callerLogIndex = (stackArr.length > 0) ? 1 : 0;
    // for (let i = 0; i < stackArr.length; i++) {
    //     if (stackArr[i].indexOf('makeError') > 0 && i + 1 < stackArr.length) {
    //         callerLogIndex = i + 1;
    //         break;
    //     }
    // }
    if (callerLogIndex !== 0) {
        const callerStackLine = stackArr[callerLogIndex];
        let list = callerStackLine.split(/\(|\)/);
        if (list[1] && list[1].length > 0) {
            return `[${list[1].replace(process.cwd(), '').replace(/\\/g, "/").replace('/', '')}]`;
        }
        else {
            return `[${callerStackLine.substring(callerStackLine.lastIndexOf(path_1.sep) + 1, callerStackLine.lastIndexOf(':'))}]`;
        }
    }
    else {
        return '[-]';
    }
}
class HandleUnit {
    constructor(symbol) {
        this.routeMap = {};
        this.beforeList = [];
        this.afterList = [];
        this.symbol = symbol || Symbol();
    }
    normalCheck(checkMap, params) {
        for (let key in checkMap) {
            let cfg = checkMap[key];
            if (cfg.required && !params.hasOwnProperty(key)) {
                // 这里没找到 抛出一个异常
                throw Error(`param [${key} required and type limit ${cfg.type}]`);
            }
            else if (!params.hasOwnProperty(key)) {
                continue;
            }
            if (cfg.type == typeof params[key]) {
                continue;
            }
            // 这里类型不同
            if (!cfg.change) {
                throw Error(`param [${key} type limit ${cfg.type}]`);
            }
            else {
                switch (cfg.type) {
                    case "bigint":
                        params[key] = BigInt(params[key]);
                        break;
                    case "boolean": {
                        if (params[key] == true || params[key] == "true") {
                            params[key] = true;
                        }
                        else {
                            params[key] = false;
                        }
                        // params[key] = Boolean(params[key]);
                        break;
                    }
                    case "function": throw Error(`param [${key} type limit ${cfg.type}]`);
                    case "number":
                        params[key] = Number(params[key]);
                        // 这里增加一个小优化 ，如果是整数那么直接转换成整数
                        if (params[key] = parseInt(params[key])) {
                            params[key] = parseInt(params[key]);
                        }
                        break;
                    case "object":
                        params[key] = JSON.parse(params[key].toString());
                        break;
                    case "string":
                        params[key] = String(params[key]);
                        break;
                    case "symbol":
                        params[key] = Symbol(params[key]);
                        break;
                    default: break;
                }
            }
        }
        return params;
    }
    paramsCheck(checkMap, params) {
        if (checkMap.params == undefined)
            return params;
        for (let i = 0; i < checkMap.params.length; i++) {
            let v = checkMap.params[i];
            switch (v.type) {
                case "class-validator":
                    break;
                case "joi":
                    // 
                    let err = v.infos.validate(params, {
                        allowUnknown: true,
                        stripUnknown: true
                    });
                    if (err.error) {
                        throw { code: -1, errMsg: err.error.message, stack: err.error.stack };
                    }
                    params = err.value;
                    break;
                default:
                    params = this.normalCheck(v.infos, params);
                    break;
            }
        }
        return params;
    }
    // 调用接口
    getRouteFunc(Name, req) {
        let routeObj = this.routeMap[Name];
        if (!routeObj) {
            return undefined;
        }
        req.params = this.paramsCheck(routeObj, req.params);
        return routeObj.value.bind(routeObj.target, req.params, req.method.toUpperCase(), req.headers, req);
    }
    /**
     * 调用模块
     * @param pathlist
     * @param req
     * @param res
     * @param next
     */
    route(Name, req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let routeFunc = this.getRouteFunc(Name, req);
                if (!routeFunc) {
                    req.responseData = {
                        code: -1,
                        errMsg: "can not find [" + req.path + "]"
                    };
                    return true;
                }
                // 这里开始调用响应
                req.responseData = yield routeFunc();
                if (req.responseData == undefined) {
                    req.responseData = { code: 0 };
                }
            }
            catch (e) {
                if (!e) {
                    req.responseData = {
                        code: -1,
                        errMsg: "error bug no message"
                    };
                }
                else if (typeof e == 'string') {
                    req.responseData = e;
                }
                else if (e instanceof Error) {
                    req.responseData = {
                        code: -1,
                        errMsg: e.message,
                        __line__: parseErrorLine(e)
                    };
                }
                else {
                    req.responseData = e;
                }
            }
            return true;
        });
    }
    /**
    * 调用模块
    * @param pathlist
    * @param req
    * @param res
    * @param next
    */
    before(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            for (let i = 0; i < this.beforeList.length; i++) {
                let routeMap = this.beforeList[i];
                try {
                    req.params = this.paramsCheck(routeMap, req.params);
                    yield routeMap.value.apply(routeMap.target, [req, res]);
                }
                catch (e) {
                    // 出钱异常的化
                    if (!e) {
                        req.responseData = {
                            code: -1,
                            errMsg: "error bug no message"
                        };
                    }
                    else if (typeof e == 'string') {
                        req.responseData = e;
                    }
                    else if (e instanceof Error) {
                        req.responseData = {
                            code: -1,
                            errMsg: e.message,
                            __line__: parseErrorLine(e)
                        };
                    }
                    else {
                        req.responseData = e;
                    }
                    return false;
                }
            }
            return true;
        });
    }
    /**
    * 调用模块
    * @param pathlist
    * @param req
    * @param res
    * @param next
    */
    after(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            for (let i = 0; i < this.afterList.length; i++) {
                let routeMap = this.afterList[i];
                try {
                    req.params = this.paramsCheck(routeMap, req.params);
                    yield routeMap.value.apply(routeMap.target, [req, res]);
                }
                catch (e) {
                    // 出钱异常的化
                    if (!e) {
                        req.responseData = {
                            code: -1,
                            errMsg: "error bug no message"
                        };
                    }
                    else if (typeof e == 'string') {
                        req.responseData = e;
                    }
                    else if (e instanceof Error) {
                        req.responseData = {
                            code: -1,
                            errMsg: e.message,
                            __line__: parseErrorLine(e)
                        };
                    }
                    else {
                        req.responseData = e;
                    }
                    return false;
                }
            }
            return true;
        });
    }
}
exports.HandleUnit = HandleUnit;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSGFuZGxlVW5pdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkhhbmRsZVVuaXQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBR0EsK0JBQTJCO0FBSzNCLFNBQVMsY0FBYyxDQUFDLEdBQVU7SUFDOUIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxZQUFZLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRTtRQUMvQyxPQUFPLEtBQUssQ0FBQTtLQUNmO0lBRUQsTUFBTSxLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztJQUN4QixNQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ25DLElBQUksY0FBYyxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbkQsOENBQThDO0lBQzlDLDZFQUE2RTtJQUM3RSxrQ0FBa0M7SUFDbEMsaUJBQWlCO0lBQ2pCLFFBQVE7SUFDUixJQUFJO0lBRUosSUFBSSxjQUFjLEtBQUssQ0FBQyxFQUFFO1FBQ3RCLE1BQU0sZUFBZSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQVcsQ0FBQztRQUMzRCxJQUFJLElBQUksR0FBRyxlQUFlLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBQ3pDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQy9CLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQTtTQUN4RjthQUNJO1lBQ0QsT0FBTyxJQUFJLGVBQWUsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsZUFBZSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUM7U0FDbkg7S0FDSjtTQUFNO1FBQ0gsT0FBTyxLQUFLLENBQUM7S0FDaEI7QUFDTCxDQUFDO0FBRUQsTUFBYSxVQUFVO0lBRW5CLFlBQVksTUFBZTtRQUkzQixhQUFRLEdBSUosRUFBRSxDQUFDO1FBQ1AsZUFBVSxHQUVILEVBQUUsQ0FBQztRQUNWLGNBQVMsR0FFRixFQUFFLENBQUM7UUFiTixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sSUFBSSxNQUFNLEVBQUUsQ0FBQTtJQUNwQyxDQUFDO0lBZUQsV0FBVyxDQUFDLFFBQTRCLEVBQUUsTUFBNEI7UUFDbEUsS0FBSyxJQUFJLEdBQUcsSUFBSSxRQUFRLEVBQUU7WUFDdEIsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3hCLElBQUksR0FBRyxDQUFDLFFBQVEsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzdDLGVBQWU7Z0JBQ2YsTUFBTSxLQUFLLENBQUMsVUFBVSxHQUFHLDRCQUE0QixHQUFHLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQTthQUNwRTtpQkFDSSxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDbEMsU0FBUzthQUNaO1lBRUQsSUFBSSxHQUFHLENBQUMsSUFBSSxJQUFJLE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNoQyxTQUFRO2FBQ1g7WUFFRCxTQUFTO1lBQ1QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2IsTUFBTSxLQUFLLENBQUMsVUFBVSxHQUFHLGVBQWUsR0FBRyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUE7YUFDdkQ7aUJBQ0k7Z0JBQ0QsUUFBUSxHQUFHLENBQUMsSUFBSSxFQUFFO29CQUNkLEtBQUssUUFBUTt3QkFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUFDLE1BQU07b0JBQ3hELEtBQUssU0FBUyxDQUFDLENBQUM7d0JBQ1osSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxJQUFJLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLEVBQUU7NEJBQzlDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7eUJBQ3RCOzZCQUNJOzRCQUNELE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUM7eUJBQ3ZCO3dCQUNELHNDQUFzQzt3QkFDdEMsTUFBTTtxQkFDVDtvQkFDRCxLQUFLLFVBQVUsQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLFVBQVUsR0FBRyxlQUFlLEdBQUcsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO29CQUN0RSxLQUFLLFFBQVE7d0JBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDN0MsNEJBQTRCO3dCQUM1QixJQUFJLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7NEJBQ3JDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7eUJBQ3RDO3dCQUNELE1BQU07b0JBQ1YsS0FBSyxRQUFRO3dCQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO3dCQUFDLE1BQU07b0JBQ3ZFLEtBQUssUUFBUTt3QkFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUFDLE1BQU07b0JBQ3hELEtBQUssUUFBUTt3QkFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUFDLE1BQU07b0JBQ3hELE9BQU8sQ0FBQyxDQUFDLE1BQU07aUJBQ2xCO2FBQ0o7U0FDSjtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxXQUFXLENBQUMsUUFBb0IsRUFBRSxNQUE0QjtRQUMxRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksU0FBUztZQUFFLE9BQU8sTUFBTSxDQUFDO1FBQ2hELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM3QyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNCLFFBQVEsQ0FBQyxDQUFDLElBQUksRUFBRTtnQkFDWixLQUFLLGlCQUFpQjtvQkFDbEIsTUFBTTtnQkFDVixLQUFLLEtBQUs7b0JBQ04sR0FBRztvQkFDSCxJQUFJLEdBQUcsR0FBSSxDQUFDLENBQUMsS0FBMEIsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFO3dCQUNyRCxZQUFZLEVBQUUsSUFBSTt3QkFDbEIsWUFBWSxFQUFFLElBQUk7cUJBQ3JCLENBQUMsQ0FBQTtvQkFDRixJQUFJLEdBQUcsQ0FBQyxLQUFLLEVBQUU7d0JBQ1gsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUE7cUJBQ3hFO29CQUNELE1BQU0sR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO29CQUNuQixNQUFNO2dCQUNWO29CQUNJLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7b0JBQzNDLE1BQU07YUFDYjtTQUNKO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVELE9BQU87SUFDUCxZQUFZLENBQUMsSUFBWSxFQUFFLEdBQWM7UUFDckMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ1gsT0FBTyxTQUFTLENBQUM7U0FDcEI7UUFDRCxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUNuRCxPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUE7SUFDdkcsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNHLEtBQUssQ0FBQyxJQUFZLEVBQUUsR0FBYyxFQUFFLEdBQWEsRUFBRSxJQUFrQjs7WUFDdkUsSUFBSTtnQkFDQSxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQTtnQkFDNUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtvQkFDWixHQUFHLENBQUMsWUFBWSxHQUFHO3dCQUNmLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ1IsTUFBTSxFQUFFLGdCQUFnQixHQUFHLEdBQUcsQ0FBQyxJQUFJLEdBQUcsR0FBRztxQkFDNUMsQ0FBQTtvQkFDRCxPQUFPLElBQUksQ0FBQztpQkFDZjtnQkFFRCxXQUFXO2dCQUNYLEdBQUcsQ0FBQyxZQUFZLEdBQUcsTUFBTSxTQUFTLEVBQUUsQ0FBQTtnQkFDcEMsSUFBSSxHQUFHLENBQUMsWUFBWSxJQUFJLFNBQVMsRUFBRTtvQkFDL0IsR0FBRyxDQUFDLFlBQVksR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQTtpQkFDakM7YUFDSjtZQUNELE9BQU8sQ0FBQyxFQUFFO2dCQUNOLElBQUksQ0FBQyxDQUFDLEVBQUU7b0JBQ0osR0FBRyxDQUFDLFlBQVksR0FBRzt3QkFDZixJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNSLE1BQU0sRUFBRSxzQkFBc0I7cUJBQ2pDLENBQUE7aUJBQ0o7cUJBQ0ksSUFBSSxPQUFPLENBQUMsSUFBSSxRQUFRLEVBQUU7b0JBQzNCLEdBQUcsQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFBO2lCQUN2QjtxQkFDSSxJQUFJLENBQUMsWUFBWSxLQUFLLEVBQUU7b0JBQ3pCLEdBQUcsQ0FBQyxZQUFZLEdBQUc7d0JBQ2YsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDUixNQUFNLEVBQUUsQ0FBQyxDQUFDLE9BQU87d0JBQ2pCLFFBQVEsRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDO3FCQUM5QixDQUFBO2lCQUNKO3FCQUNJO29CQUNELEdBQUcsQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO2lCQUN4QjthQUNKO1lBRUQsT0FBTyxJQUFJLENBQUE7UUFDZixDQUFDO0tBQUE7SUFDRDs7Ozs7O01BTUU7SUFDSSxNQUFNLENBQUMsR0FBYyxFQUFFLEdBQWE7O1lBQ3RDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDN0MsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsSUFBSTtvQkFDQSxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDcEQsTUFBTSxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQzNEO2dCQUNELE9BQU8sQ0FBQyxFQUFFO29CQUNOLFNBQVM7b0JBQ1QsSUFBSSxDQUFDLENBQUMsRUFBRTt3QkFDSixHQUFHLENBQUMsWUFBWSxHQUFHOzRCQUNmLElBQUksRUFBRSxDQUFDLENBQUM7NEJBQ1IsTUFBTSxFQUFFLHNCQUFzQjt5QkFDakMsQ0FBQTtxQkFDSjt5QkFDSSxJQUFJLE9BQU8sQ0FBQyxJQUFJLFFBQVEsRUFBRTt3QkFDM0IsR0FBRyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUE7cUJBQ3ZCO3lCQUNJLElBQUksQ0FBQyxZQUFZLEtBQUssRUFBRTt3QkFDekIsR0FBRyxDQUFDLFlBQVksR0FBRzs0QkFDZixJQUFJLEVBQUUsQ0FBQyxDQUFDOzRCQUNSLE1BQU0sRUFBRSxDQUFDLENBQUMsT0FBTzs0QkFDakIsUUFBUSxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUM7eUJBQzlCLENBQUE7cUJBQ0o7eUJBQ0k7d0JBQ0QsR0FBRyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7cUJBQ3hCO29CQUNELE9BQU8sS0FBSyxDQUFBO2lCQUNmO2FBQ0o7WUFDRCxPQUFPLElBQUksQ0FBQTtRQUNmLENBQUM7S0FBQTtJQUNEOzs7Ozs7TUFNRTtJQUNJLEtBQUssQ0FBQyxHQUFjLEVBQUUsR0FBYTs7WUFDckMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM1QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxJQUFJO29CQUNBLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNwRCxNQUFNLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDM0Q7Z0JBQ0QsT0FBTyxDQUFDLEVBQUU7b0JBQ04sU0FBUztvQkFDVCxJQUFJLENBQUMsQ0FBQyxFQUFFO3dCQUNKLEdBQUcsQ0FBQyxZQUFZLEdBQUc7NEJBQ2YsSUFBSSxFQUFFLENBQUMsQ0FBQzs0QkFDUixNQUFNLEVBQUUsc0JBQXNCO3lCQUNqQyxDQUFBO3FCQUNKO3lCQUNJLElBQUksT0FBTyxDQUFDLElBQUksUUFBUSxFQUFFO3dCQUMzQixHQUFHLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQTtxQkFDdkI7eUJBQ0ksSUFBSSxDQUFDLFlBQVksS0FBSyxFQUFFO3dCQUN6QixHQUFHLENBQUMsWUFBWSxHQUFHOzRCQUNmLElBQUksRUFBRSxDQUFDLENBQUM7NEJBQ1IsTUFBTSxFQUFFLENBQUMsQ0FBQyxPQUFPOzRCQUNqQixRQUFRLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQzt5QkFDOUIsQ0FBQTtxQkFDSjt5QkFDSTt3QkFDRCxHQUFHLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztxQkFDeEI7b0JBQ0QsT0FBTyxLQUFLLENBQUE7aUJBQ2Y7YUFDSjtZQUNELE9BQU8sSUFBSSxDQUFBO1FBQ2YsQ0FBQztLQUFBO0NBQ0o7QUExT0QsZ0NBME9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmV4dEZ1bmN0aW9uLCBSZXF1ZXN0LFJlc3BvbnNlIH0gZnJvbSBcImV4cHJlc3NcIjtcbmltcG9ydCB7IEluY29taW5nSHR0cEhlYWRlcnMgfSBmcm9tIFwiaHR0cFwiO1xuaW1wb3J0IEpvaSBmcm9tIFwiam9pXCI7XG5pbXBvcnQgeyBzZXAgfSBmcm9tIFwicGF0aFwiO1xuaW1wb3J0IHsgaWZQYXJhbUNoZWNrQ29uZmlnLCBJUm91dGVCYXNlLCBSZXF1ZXN0RXggfSBmcm9tIFwiLi9kZWZpbmVcIjtcblxuXG5cbmZ1bmN0aW9uIHBhcnNlRXJyb3JMaW5lKGVycjogRXJyb3IpIHtcbiAgICBpZiAoIWVyciB8fCAhKGVyciBpbnN0YW5jZW9mIEVycm9yKSB8fCAhZXJyLnN0YWNrKSB7XG4gICAgICAgIHJldHVybiAnWy1dJ1xuICAgIH1cblxuICAgIGNvbnN0IHN0YWNrID0gZXJyLnN0YWNrO1xuICAgIGNvbnN0IHN0YWNrQXJyID0gc3RhY2suc3BsaXQoJ1xcbicpO1xuICAgIGxldCBjYWxsZXJMb2dJbmRleCA9IChzdGFja0Fyci5sZW5ndGggPiAwKSA/IDEgOiAwO1xuICAgIC8vIGZvciAobGV0IGkgPSAwOyBpIDwgc3RhY2tBcnIubGVuZ3RoOyBpKyspIHtcbiAgICAvLyAgICAgaWYgKHN0YWNrQXJyW2ldLmluZGV4T2YoJ21ha2VFcnJvcicpID4gMCAmJiBpICsgMSA8IHN0YWNrQXJyLmxlbmd0aCkge1xuICAgIC8vICAgICAgICAgY2FsbGVyTG9nSW5kZXggPSBpICsgMTtcbiAgICAvLyAgICAgICAgIGJyZWFrO1xuICAgIC8vICAgICB9XG4gICAgLy8gfVxuXG4gICAgaWYgKGNhbGxlckxvZ0luZGV4ICE9PSAwKSB7XG4gICAgICAgIGNvbnN0IGNhbGxlclN0YWNrTGluZSA9IHN0YWNrQXJyW2NhbGxlckxvZ0luZGV4XSBhcyBzdHJpbmc7XG4gICAgICAgIGxldCBsaXN0ID0gY2FsbGVyU3RhY2tMaW5lLnNwbGl0KC9cXCh8XFwpLylcbiAgICAgICAgaWYgKGxpc3RbMV0gJiYgbGlzdFsxXS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICByZXR1cm4gYFske2xpc3RbMV0ucmVwbGFjZShwcm9jZXNzLmN3ZCgpLCAnJykucmVwbGFjZSgvXFxcXC9nLCBcIi9cIikucmVwbGFjZSgnLycsICcnKX1dYFxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIGBbJHtjYWxsZXJTdGFja0xpbmUuc3Vic3RyaW5nKGNhbGxlclN0YWNrTGluZS5sYXN0SW5kZXhPZihzZXApICsgMSwgY2FsbGVyU3RhY2tMaW5lLmxhc3RJbmRleE9mKCc6JykpfV1gO1xuICAgICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuICdbLV0nO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIEhhbmRsZVVuaXQge1xuICAgIHN5bWJvbDogc3ltYm9sXG4gICAgY29uc3RydWN0b3Ioc3ltYm9sPzogc3ltYm9sKSB7XG4gICAgICAgIHRoaXMuc3ltYm9sID0gc3ltYm9sIHx8IFN5bWJvbCgpXG4gICAgfVxuXG4gICAgcm91dGVNYXA6IHtcbiAgICAgICAgW2Y6IHN0cmluZ106IElSb3V0ZUJhc2UgJiB7XG4gICAgICAgICAgICB2YWx1ZTogKHBhcmFtOiB7IFt4OiBzdHJpbmddOiBhbnkgfSwgbWV0aG9kOiBzdHJpbmcsIGhlYWRlcnM6IEluY29taW5nSHR0cEhlYWRlcnMsIHJlcT86IFJlcXVlc3RFeCkgPT4gUHJvbWlzZTxhbnk+O1xuICAgICAgICB9XG4gICAgfSA9IHt9O1xuICAgIGJlZm9yZUxpc3Q6IChJUm91dGVCYXNlICYge1xuICAgICAgICB2YWx1ZTogKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSkgPT4gYW55XG4gICAgfSlbXSA9IFtdO1xuICAgIGFmdGVyTGlzdDogKElSb3V0ZUJhc2UgJiB7XG4gICAgICAgIHZhbHVlOiAocmVxOiBSZXF1ZXN0LCByZXM6IFJlc3BvbnNlKSA9PiBhbnlcbiAgICB9KVtdID0gW107XG5cblxuICAgIG5vcm1hbENoZWNrKGNoZWNrTWFwOiBpZlBhcmFtQ2hlY2tDb25maWcsIHBhcmFtczogeyBbeDogc3RyaW5nXTogYW55IH0pIHtcbiAgICAgICAgZm9yIChsZXQga2V5IGluIGNoZWNrTWFwKSB7XG4gICAgICAgICAgICBsZXQgY2ZnID0gY2hlY2tNYXBba2V5XTtcbiAgICAgICAgICAgIGlmIChjZmcucmVxdWlyZWQgJiYgIXBhcmFtcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgICAgICAgLy8g6L+Z6YeM5rKh5om+5YiwIOaKm+WHuuS4gOS4quW8guW4uFxuICAgICAgICAgICAgICAgIHRocm93IEVycm9yKGBwYXJhbSBbJHtrZXl9IHJlcXVpcmVkIGFuZCB0eXBlIGxpbWl0ICR7Y2ZnLnR5cGV9XWApXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICghcGFyYW1zLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKGNmZy50eXBlID09IHR5cGVvZiBwYXJhbXNba2V5XSkge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIOi/memHjOexu+Wei+S4jeWQjFxuICAgICAgICAgICAgaWYgKCFjZmcuY2hhbmdlKSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgRXJyb3IoYHBhcmFtIFske2tleX0gdHlwZSBsaW1pdCAke2NmZy50eXBlfV1gKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChjZmcudHlwZSkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlIFwiYmlnaW50XCI6IHBhcmFtc1trZXldID0gQmlnSW50KHBhcmFtc1trZXldKTsgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgXCJib29sZWFuXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwYXJhbXNba2V5XSA9PSB0cnVlIHx8IHBhcmFtc1trZXldID09IFwidHJ1ZVwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zW2tleV0gPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zW2tleV0gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHBhcmFtc1trZXldID0gQm9vbGVhbihwYXJhbXNba2V5XSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwiZnVuY3Rpb25cIjogdGhyb3cgRXJyb3IoYHBhcmFtIFske2tleX0gdHlwZSBsaW1pdCAke2NmZy50eXBlfV1gKTtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBcIm51bWJlclwiOiBwYXJhbXNba2V5XSA9IE51bWJlcihwYXJhbXNba2V5XSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyDov5nph4zlop7liqDkuIDkuKrlsI/kvJjljJYg77yM5aaC5p6c5piv5pW05pWw6YKj5LmI55u05o6l6L2s5o2i5oiQ5pW05pWwXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocGFyYW1zW2tleV0gPSBwYXJzZUludChwYXJhbXNba2V5XSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXNba2V5XSA9IHBhcnNlSW50KHBhcmFtc1trZXldKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgXCJvYmplY3RcIjogcGFyYW1zW2tleV0gPSBKU09OLnBhcnNlKHBhcmFtc1trZXldLnRvU3RyaW5nKCkpOyBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBcInN0cmluZ1wiOiBwYXJhbXNba2V5XSA9IFN0cmluZyhwYXJhbXNba2V5XSk7IGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIFwic3ltYm9sXCI6IHBhcmFtc1trZXldID0gU3ltYm9sKHBhcmFtc1trZXldKTsgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6IGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBwYXJhbXM7XG4gICAgfVxuXG4gICAgcGFyYW1zQ2hlY2soY2hlY2tNYXA6IElSb3V0ZUJhc2UsIHBhcmFtczogeyBbeDogc3RyaW5nXTogYW55IH0pIHtcbiAgICAgICAgaWYgKGNoZWNrTWFwLnBhcmFtcyA9PSB1bmRlZmluZWQpIHJldHVybiBwYXJhbXM7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hlY2tNYXAucGFyYW1zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgdiA9IGNoZWNrTWFwLnBhcmFtc1tpXTtcbiAgICAgICAgICAgIHN3aXRjaCAodi50eXBlKSB7XG4gICAgICAgICAgICAgICAgY2FzZSBcImNsYXNzLXZhbGlkYXRvclwiOlxuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIFwiam9pXCI6XG4gICAgICAgICAgICAgICAgICAgIC8vIFxuICAgICAgICAgICAgICAgICAgICBsZXQgZXJyID0gKHYuaW5mb3MgYXMgSm9pLk9iamVjdFNjaGVtYSkudmFsaWRhdGUocGFyYW1zLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhbGxvd1Vua25vd246IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHJpcFVua25vd246IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVyci5lcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgeyBjb2RlOiAtMSwgZXJyTXNnOiBlcnIuZXJyb3IubWVzc2FnZSwgc3RhY2s6IGVyci5lcnJvci5zdGFjayB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zID0gZXJyLnZhbHVlO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICBwYXJhbXMgPSB0aGlzLm5vcm1hbENoZWNrKHYuaW5mb3MsIHBhcmFtcyk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHBhcmFtcztcbiAgICB9XG5cbiAgICAvLyDosIPnlKjmjqXlj6NcbiAgICBnZXRSb3V0ZUZ1bmMoTmFtZTogc3RyaW5nLCByZXE6IFJlcXVlc3RFeCkge1xuICAgICAgICBsZXQgcm91dGVPYmogPSB0aGlzLnJvdXRlTWFwW05hbWVdO1xuICAgICAgICBpZiAoIXJvdXRlT2JqKSB7XG4gICAgICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xuICAgICAgICB9XG4gICAgICAgIHJlcS5wYXJhbXMgPSB0aGlzLnBhcmFtc0NoZWNrKHJvdXRlT2JqLCByZXEucGFyYW1zKVxuICAgICAgICByZXR1cm4gcm91dGVPYmoudmFsdWUuYmluZChyb3V0ZU9iai50YXJnZXQsIHJlcS5wYXJhbXMsIHJlcS5tZXRob2QudG9VcHBlckNhc2UoKSwgcmVxLmhlYWRlcnMsIHJlcSlcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDosIPnlKjmqKHlnZdcbiAgICAgKiBAcGFyYW0gcGF0aGxpc3QgXG4gICAgICogQHBhcmFtIHJlcSBcbiAgICAgKiBAcGFyYW0gcmVzIFxuICAgICAqIEBwYXJhbSBuZXh0IFxuICAgICAqL1xuICAgIGFzeW5jIHJvdXRlKE5hbWU6IHN0cmluZywgcmVxOiBSZXF1ZXN0RXgsIHJlczogUmVzcG9uc2UsIG5leHQ6IE5leHRGdW5jdGlvbikge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgbGV0IHJvdXRlRnVuYyA9IHRoaXMuZ2V0Um91dGVGdW5jKE5hbWUsIHJlcSlcbiAgICAgICAgICAgIGlmICghcm91dGVGdW5jKSB7XG4gICAgICAgICAgICAgICAgcmVxLnJlc3BvbnNlRGF0YSA9IHtcbiAgICAgICAgICAgICAgICAgICAgY29kZTogLTEsXG4gICAgICAgICAgICAgICAgICAgIGVyck1zZzogXCJjYW4gbm90IGZpbmQgW1wiICsgcmVxLnBhdGggKyBcIl1cIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8g6L+Z6YeM5byA5aeL6LCD55So5ZON5bqUXG4gICAgICAgICAgICByZXEucmVzcG9uc2VEYXRhID0gYXdhaXQgcm91dGVGdW5jKClcbiAgICAgICAgICAgIGlmIChyZXEucmVzcG9uc2VEYXRhID09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHJlcS5yZXNwb25zZURhdGEgPSB7IGNvZGU6IDAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChlKSB7XG4gICAgICAgICAgICBpZiAoIWUpIHtcbiAgICAgICAgICAgICAgICByZXEucmVzcG9uc2VEYXRhID0ge1xuICAgICAgICAgICAgICAgICAgICBjb2RlOiAtMSxcbiAgICAgICAgICAgICAgICAgICAgZXJyTXNnOiBcImVycm9yIGJ1ZyBubyBtZXNzYWdlXCJcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICh0eXBlb2YgZSA9PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICAgIHJlcS5yZXNwb25zZURhdGEgPSBlXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmIChlIGluc3RhbmNlb2YgRXJyb3IpIHtcbiAgICAgICAgICAgICAgICByZXEucmVzcG9uc2VEYXRhID0ge1xuICAgICAgICAgICAgICAgICAgICBjb2RlOiAtMSxcbiAgICAgICAgICAgICAgICAgICAgZXJyTXNnOiBlLm1lc3NhZ2UsXG4gICAgICAgICAgICAgICAgICAgIF9fbGluZV9fOiBwYXJzZUVycm9yTGluZShlKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJlcS5yZXNwb25zZURhdGEgPSBlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICB9XG4gICAgLyoqXG4gICAgKiDosIPnlKjmqKHlnZdcbiAgICAqIEBwYXJhbSBwYXRobGlzdCBcbiAgICAqIEBwYXJhbSByZXEgXG4gICAgKiBAcGFyYW0gcmVzIFxuICAgICogQHBhcmFtIG5leHQgXG4gICAgKi9cbiAgICBhc3luYyBiZWZvcmUocmVxOiBSZXF1ZXN0RXgsIHJlczogUmVzcG9uc2UpIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmJlZm9yZUxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGxldCByb3V0ZU1hcCA9IHRoaXMuYmVmb3JlTGlzdFtpXTtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgcmVxLnBhcmFtcyA9IHRoaXMucGFyYW1zQ2hlY2socm91dGVNYXAsIHJlcS5wYXJhbXMpO1xuICAgICAgICAgICAgICAgIGF3YWl0IHJvdXRlTWFwLnZhbHVlLmFwcGx5KHJvdXRlTWFwLnRhcmdldCwgW3JlcSwgcmVzXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIC8vIOWHuumSseW8guW4uOeahOWMllxuICAgICAgICAgICAgICAgIGlmICghZSkge1xuICAgICAgICAgICAgICAgICAgICByZXEucmVzcG9uc2VEYXRhID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogLTEsXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJNc2c6IFwiZXJyb3IgYnVnIG5vIG1lc3NhZ2VcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBlID09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlcS5yZXNwb25zZURhdGEgPSBlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKGUgaW5zdGFuY2VvZiBFcnJvcikge1xuICAgICAgICAgICAgICAgICAgICByZXEucmVzcG9uc2VEYXRhID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogLTEsXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJNc2c6IGUubWVzc2FnZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIF9fbGluZV9fOiBwYXJzZUVycm9yTGluZShlKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXEucmVzcG9uc2VEYXRhID0gZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICB9XG4gICAgLyoqXG4gICAgKiDosIPnlKjmqKHlnZdcbiAgICAqIEBwYXJhbSBwYXRobGlzdCBcbiAgICAqIEBwYXJhbSByZXEgXG4gICAgKiBAcGFyYW0gcmVzIFxuICAgICogQHBhcmFtIG5leHQgXG4gICAgKi9cbiAgICBhc3luYyBhZnRlcihyZXE6IFJlcXVlc3RFeCwgcmVzOiBSZXNwb25zZSkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuYWZ0ZXJMaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgcm91dGVNYXAgPSB0aGlzLmFmdGVyTGlzdFtpXTtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgcmVxLnBhcmFtcyA9IHRoaXMucGFyYW1zQ2hlY2socm91dGVNYXAsIHJlcS5wYXJhbXMpO1xuICAgICAgICAgICAgICAgIGF3YWl0IHJvdXRlTWFwLnZhbHVlLmFwcGx5KHJvdXRlTWFwLnRhcmdldCwgW3JlcSwgcmVzXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIC8vIOWHuumSseW8guW4uOeahOWMllxuICAgICAgICAgICAgICAgIGlmICghZSkge1xuICAgICAgICAgICAgICAgICAgICByZXEucmVzcG9uc2VEYXRhID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogLTEsXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJNc2c6IFwiZXJyb3IgYnVnIG5vIG1lc3NhZ2VcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBlID09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlcS5yZXNwb25zZURhdGEgPSBlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKGUgaW5zdGFuY2VvZiBFcnJvcikge1xuICAgICAgICAgICAgICAgICAgICByZXEucmVzcG9uc2VEYXRhID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogLTEsXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJNc2c6IGUubWVzc2FnZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIF9fbGluZV9fOiBwYXJzZUVycm9yTGluZShlKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXEucmVzcG9uc2VEYXRhID0gZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICB9XG59XG4iXX0=