import { Request } from "express";
import Joi from "joi";



export interface RequestEx extends Request {
    responseData: any
}

export type SeParamConfigType = "bigint" | "boolean" | "function" | "number" | "object" | "string" | "symbol"

export interface ifCheckObject {
    type: "normal" | "joi" | "class-validator",
    infos: any
}

export interface ifParamCheckConfig {
    [name: string]: {
        type: SeParamConfigType,
        required: boolean, change: boolean
    }
}
export interface TypedPropertyDescriptorEx<T> extends TypedPropertyDescriptor<T> {
    params?: {
        joi?: Joi.ObjectSchema,
        normal: ifParamCheckConfig
    }
}


export interface IRouteBase {
    target: any;
    params: ifCheckObject[],
}