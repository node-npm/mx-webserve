import Express, { Request, Response, NextFunction } from "express";
import bodyParser from "body-parser";
import { readFileSync } from "fs";
import { join, parse } from "path";
import { gzipSync } from "zlib";

import compression from 'compression'

import { debug } from "debug";
import proxy from "express-http-proxy";
let Debug = debug("mx-webserve");


let gRoutePath = (process as any).mainModule ? parse((process as any).mainModule.filename).dir : process.cwd();

export interface IProxyOptions extends proxy.ProxyOptions {

}

export class WebRouteUnit {
    constructor() {
    }

    app: Express.Application = Express();
    public use(...handlers: any[]) {
        if (this.app) this.app.use(...handlers);
    }

    // 静态资源
    static(pre: string, root: string) {
        return this.app.use(pre, Express.static<any>(root, {}))
    }

    // 增加代理接口
    proxy(pre: string, host: string | ((req: Request) => string), options?: IProxyOptions) {
        return this.app.use(pre, proxy(host, options))
    }

    /**
     * 开启配置
     * @param setting 
     */
    enable(setting: string) {
        this.app.enable(setting)
    }

    /**
     * 关闭配置
     * @param setting 
     */
    disable(setting: string) {
        this.app.disable(setting)
    }

    private _crossUse(req: Request, res: Response, next: NextFunction) {
        if (this.app.enabled("cross")) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            res.header("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
        }
        next();
    }

    private _options(req: Request, res: Response, next: NextFunction) {
        res.end();
    }

    private _404(req: Request, res: Response, next: NextFunction) {
        if (!res.headersSent) {
            // 表示有人处理过了
            res.writeHead(404);
            res.write(
                JSON.stringify({
                    code: -1,
                    errMsg: "can not find [" + req.path + "]"
                })
            );
        }
        res.end();
    }

    public init(port: number, uses: any[], limit?: string | { json?: string, text?: string, raw?: string, urlencoded?: string, gzip?: boolean, [x: string]: string | boolean | undefined }) {
        if (this.app.enabled("init")) return Promise.resolve()
        this.app.use(this._crossUse.bind(this));
        this.app.options("*", this._options.bind(this));

        if (typeof limit == 'string') {
            limit = {
                text: limit,
                json: limit,
                urlencoded: limit,
                raw: limit,
            }
        }
        else if (limit == undefined) {
            limit = {};
        }

        this.app.use(bodyParser.text({ limit: limit.text || '100kb' }));
        this.app.use(bodyParser.raw({ limit: limit.raw || '100kb' }));
        this.app.use(bodyParser.urlencoded({ extended: true, limit: limit.urlencoded || "10mb" }));
        this.app.use(bodyParser.json({ limit: limit.json || '100kb' }));
        if (limit.gzip == true) this.app.use(compression());

        // 注册一下别人提交的内容

        this.app.use(this.favicon.bind(this));
        for (let i = 0; i < uses.length; i++) {
            this.app.use(uses[i])
        }

        this.app.use(this._404.bind(this));

        this.load_favicon();
        this.app.enable("init");
        let server = this.app.listen(port);
        return new Promise<void>((resolve, reject) => {
            // 建立一个web监控
            server.on("error", function (e) {
                Debug(e);
                reject(e)
            });

            server.on("listening", () => {
                Debug("listen on:" + port);
                resolve()
            });
        })
    }

    private favicon_data!: Buffer;
    load_favicon() {
        try {
            this.favicon_data = gzipSync(
                readFileSync(join(gRoutePath, "favicon.ico"))
            );
        } catch (e) { }
    }

    private favicon(req: Express.Request, res: Express.Response, next: Express.NextFunction) {
        if (req.url == "/favicon.ico") {
            if (!this.favicon_data) {
                res.status(404)
            }
            else {
                res.writeHead(200, { "Content-Type": "image/x-icon", "Content-Encoding": "gzip" });
                res.write(this.favicon_data);
            }
            res.end();
        } else {
            next();
        }
    }
}
