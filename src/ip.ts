import os from "os"

function IPV4ToNumber(ip: string) {
    let ips = ip.split('.');
    let num = BigInt(0);
    for (let i = 0; i < ips.length; i++) {
        num |= BigInt(ips[i]) << BigInt((ips.length - 1 - i) * 8);
    }

    // console.log(num);
    return num;
}

function IPV6ToNumber(ip: string) {
    let ips = ip.split(':');
    let num = BigInt(0);
    for (let i = 0; i < ips.length; i++) {
        let sNum = ips[i];
        if (sNum.length == 0) {
            sNum = "0x0"
        }
        else {
            sNum = "0x" + ips[i]
        }
        num |= BigInt(sNum) << BigInt((ips.length - 1 - i) * 16);
    }

    // console.log(num);
    return num;
}

function findHost(type?: "IPv4" | "IPv6") {
    let nets = os.networkInterfaces();
    // console.log(nets);
    let ips: string[] = []
    for (let key in nets) {
        let interfaces = nets[key];
        if (!interfaces) continue;
        for (let i = 0; i < interfaces.length; i++) {
            let info = interfaces[i];
            // 不找内部接口
            if (info.internal) continue;
            if (type && info.family != type) continue;
            // 只找ipv4的
            if (info.family == "IPv4") {
                // 不找自己分配的ip (暂定是子域中的第一个)

                let ip = IPV4ToNumber(info.address);
                let mask = IPV4ToNumber(info.netmask);
                let subIP = ip & ~mask;
                if (subIP == BigInt(1)) continue;
                // console.log(ip)

            } else if (info.family == "IPv6") {
                // 不找自己分配的ip (暂定是子域中的第一个)
                let ip = IPV6ToNumber(info.address);
                let mask = IPV6ToNumber(info.netmask);
                let subIP = ip & ~mask;
                if (subIP == BigInt(1) || subIP == BigInt(0)) continue;
                // console.log(ip)
            }

            ips.push(info.address);
            // console.log(info.address);
        }
    }

    return ips;
}

export function IPFindOne(type?: "IPv4" | "IPv6") {
    let ips = findHost(type);
    return (ips.length == 0) ? "localhost" : ips[0];
}

export function IPFindMany(type?: "IPv4" | "IPv6") {
    let ips = findHost(type);
    return (ips.length == 0) ? ["localhost"] : ips;
}

// findOne()