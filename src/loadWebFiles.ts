import { readFileSync, readdirSync, stat, statSync } from "fs";
import { join, parse } from "path";

// 这里提供加载所有web模块的能力
export function loadModule(dir: string) {
    loadDir(dir)
}

function loadDir(dir: string) {
    let files = readdirSync(dir)
    for (let i = 0; i < files.length; i++) {
        let target = join(dir, files[i])
        if (statSync(target).isDirectory()) {
            loadDir(target)
        }
        else {
            loadFile(target)
        }
    }

}

let loadSet = new Set(); 

function loadFile(filePath: string) {
    let lPath = parse(filePath);
    let mPath = join(lPath.dir, lPath.name)
    try {
        if (loadSet.has(mPath)) return;
        require(mPath)
        loadSet.add(mPath)
    }
    catch (e) {
        if(lPath.ext == ".ts" || lPath.ext == ".js"){
            console.error(e)
        }
    }
}