"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.start = void 0;
// 合并参数
function start(req, res, next) {
    // 这里处理一下传递参数的问题统一放在 req.params 上面
    if (typeof req.body == 'string')
        req.body = { __body: req.body };
    if (typeof req.query == 'string')
        req.query = { __query: req.query };
    req.params = Object.assign(req.params, req.query || {}, req.body || {});
    // res.setHeader("Content-Type", "text/html; charset=utf-8");
    for (let key in req.params)
        if (req.params[key] == "undefined" || req.params[key] == "null") {
            delete req.params[key];
        }
    envStart(this, req, res, next);
}
exports.start = start;
// 平台开始
function envStart(webHandle, req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let succ = yield webHandle.getEnvModule().before(req, res);
        if (!succ) {
            // 如果失败了，那么直接返回结果好了 跳过后续几个步骤
            finish(req, res, next);
        }
        else {
            // 开始路由模块启动
            globalStart(webHandle, req, res, next);
        }
    });
}
// 平台开始
function globalStart(webHandle, req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let succ = yield webHandle.getGlobalModule().before(req, res);
        if (!succ) {
            // 如果失败了，那么直接返回结果好了 跳过后续几个步骤
            finish(req, res, next);
        }
        else {
            // 开始路由模块启动
            route(webHandle, req, res, next);
        }
    });
}
// 每个模块的后置调用
function route(webHandle, req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let list = req.path.split("/");
        let name = list.pop();
        // 这里需要吸收掉多余的空
        for (let i = 0; i < list.length;) {
            if (list[i] == "") {
                list.splice(i, 1);
                continue;
            }
            i++;
        }
        let mod = list.join("/");
        let md = webHandle.getModule(mod);
        // 表示请求过程中出现意外了跳过route了
        if (yield md.before(req, res)) {
            yield md.route(name || "index", req, res, next);
            // after 调用成功的化走下一个流程
            yield md.after(req, res);
            globalAfter(webHandle, req, res, next);
        }
        else {
            globalAfter(webHandle, req, res, next);
        }
    });
}
function globalAfter(webHandle, req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        yield webHandle.getGlobalModule().after(req, res);
        // 如果失败了，那么直接返回结果好了 跳过后续几个步骤
        envAfter(webHandle, req, res, next);
    });
}
// 每个模块的后置调用
function envAfter(webHandle, req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        yield webHandle.getEnvModule().after(req, res);
        // 如果失败了，那么直接返回结果好了 跳过后续几个步骤
        finish(req, res, next);
    });
}
// 模块的后置调用
function finish(req, res, next) {
    if (req.responseData == undefined)
        res.status(404);
    else
        res.status(200).send(req.responseData);
    res.end();
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uUm91dGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjb21tb25Sb3V0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFHQSxPQUFPO0FBQ1AsU0FBZ0IsS0FBSyxDQUFrQixHQUFvQixFQUFFLEdBQXFCLEVBQUUsSUFBMEI7SUFDMUcsa0NBQWtDO0lBQ2xDLElBQUksT0FBTyxHQUFHLENBQUMsSUFBSSxJQUFJLFFBQVE7UUFBRSxHQUFHLENBQUMsSUFBSSxHQUFHLEVBQUUsTUFBTSxFQUFFLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtJQUNoRSxJQUFJLE9BQU8sR0FBRyxDQUFDLEtBQUssSUFBSSxRQUFRO1FBQUUsR0FBRyxDQUFDLEtBQUssR0FBRyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUE7SUFFcEUsR0FBRyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssSUFBSSxFQUFFLEVBQUUsR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQztJQUV4RSw2REFBNkQ7SUFFN0QsS0FBSyxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTTtRQUN0QixJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksV0FBVyxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxFQUFFO1lBQzdELE9BQU8sR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUMxQjtJQUVMLFFBQVEsQ0FBQyxJQUFJLEVBQUUsR0FBVSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztBQUMxQyxDQUFDO0FBZkQsc0JBZUM7QUFFRCxPQUFPO0FBQ1AsU0FBZSxRQUFRLENBQUMsU0FBb0IsRUFBRSxHQUFjLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7UUFDM0csSUFBSSxJQUFJLEdBQUcsTUFBTSxTQUFTLENBQUMsWUFBWSxFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1AsNEJBQTRCO1lBQzVCLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFBO1NBQ3pCO2FBQ0k7WUFDRCxXQUFXO1lBQ1gsV0FBVyxDQUFDLFNBQVMsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFBO1NBQ3pDO0lBQ0wsQ0FBQztDQUFBO0FBRUQsT0FBTztBQUNQLFNBQWUsV0FBVyxDQUFDLFNBQW9CLEVBQUUsR0FBYyxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1FBQzlHLElBQUksSUFBSSxHQUFHLE1BQU0sU0FBUyxDQUFDLGVBQWUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNQLDRCQUE0QjtZQUM1QixNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQTtTQUN6QjthQUNJO1lBQ0QsV0FBVztZQUNYLEtBQUssQ0FBQyxTQUFTLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQTtTQUNuQztJQUNMLENBQUM7Q0FBQTtBQUVELFlBQVk7QUFDWixTQUFlLEtBQUssQ0FBQyxTQUFvQixFQUFFLEdBQWMsRUFBRSxHQUFxQixFQUFFLElBQTBCOztRQUN4RyxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMvQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUE7UUFDckIsY0FBYztRQUNkLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHO1lBQzlCLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtnQkFDZixJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtnQkFDakIsU0FBUTthQUNYO1lBQ0QsQ0FBQyxFQUFFLENBQUE7U0FDTjtRQUVELElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7UUFDeEIsSUFBSSxFQUFFLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNqQyx1QkFBdUI7UUFDdkIsSUFBSSxNQUFNLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFO1lBQzNCLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksT0FBTyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUE7WUFDL0MscUJBQXFCO1lBQ3JCLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUE7WUFDeEIsV0FBVyxDQUFDLFNBQVMsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFBO1NBQ3pDO2FBQ0k7WUFDRCxXQUFXLENBQUMsU0FBUyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUE7U0FDekM7SUFDTCxDQUFDO0NBQUE7QUFFRCxTQUFlLFdBQVcsQ0FBQyxTQUFvQixFQUFFLEdBQWMsRUFBRSxHQUFxQixFQUFFLElBQTBCOztRQUM5RyxNQUFNLFNBQVMsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2xELDRCQUE0QjtRQUM1QixRQUFRLENBQUMsU0FBUyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUE7SUFDdkMsQ0FBQztDQUFBO0FBRUQsWUFBWTtBQUNaLFNBQWUsUUFBUSxDQUFDLFNBQW9CLEVBQUUsR0FBYyxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1FBQzNHLE1BQU0sU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDL0MsNEJBQTRCO1FBQzVCLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFBO0lBQzFCLENBQUM7Q0FBQTtBQUVELFVBQVU7QUFDVixTQUFTLE1BQU0sQ0FBQyxHQUFjLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjtJQUM3RSxJQUFJLEdBQUcsQ0FBQyxZQUFZLElBQUksU0FBUztRQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7O1FBQzlDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtJQUMzQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUE7QUFDYixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEV4cHJlc3MgZnJvbSBcImV4cHJlc3NcIjtcbmltcG9ydCB7IFJlcXVlc3RFeCwgV2ViSGFuZGxlIH0gZnJvbSBcIi4vd2ViSGFuZGxlXCI7XG5cbi8vIOWQiOW5tuWPguaVsFxuZXhwb3J0IGZ1bmN0aW9uIHN0YXJ0KHRoaXM6IFdlYkhhbmRsZSwgcmVxOiBFeHByZXNzLlJlcXVlc3QsIHJlczogRXhwcmVzcy5SZXNwb25zZSwgbmV4dDogRXhwcmVzcy5OZXh0RnVuY3Rpb24pIHtcbiAgICAvLyDov5nph4zlpITnkIbkuIDkuIvkvKDpgJLlj4LmlbDnmoTpl67popjnu5/kuIDmlL7lnKggcmVxLnBhcmFtcyDkuIrpnaJcbiAgICBpZiAodHlwZW9mIHJlcS5ib2R5ID09ICdzdHJpbmcnKSByZXEuYm9keSA9IHsgX19ib2R5OiByZXEuYm9keSB9XG4gICAgaWYgKHR5cGVvZiByZXEucXVlcnkgPT0gJ3N0cmluZycpIHJlcS5xdWVyeSA9IHsgX19xdWVyeTogcmVxLnF1ZXJ5IH1cblxuICAgIHJlcS5wYXJhbXMgPSBPYmplY3QuYXNzaWduKHJlcS5wYXJhbXMsIHJlcS5xdWVyeSB8fCB7fSwgcmVxLmJvZHkgfHwge30pO1xuXG4gICAgLy8gcmVzLnNldEhlYWRlcihcIkNvbnRlbnQtVHlwZVwiLCBcInRleHQvaHRtbDsgY2hhcnNldD11dGYtOFwiKTtcblxuICAgIGZvciAobGV0IGtleSBpbiByZXEucGFyYW1zKVxuICAgICAgICBpZiAocmVxLnBhcmFtc1trZXldID09IFwidW5kZWZpbmVkXCIgfHwgcmVxLnBhcmFtc1trZXldID09IFwibnVsbFwiKSB7XG4gICAgICAgICAgICBkZWxldGUgcmVxLnBhcmFtc1trZXldO1xuICAgICAgICB9XG5cbiAgICBlbnZTdGFydCh0aGlzLCByZXEgYXMgYW55LCByZXMsIG5leHQpO1xufVxuXG4vLyDlubPlj7DlvIDlp4tcbmFzeW5jIGZ1bmN0aW9uIGVudlN0YXJ0KHdlYkhhbmRsZTogV2ViSGFuZGxlLCByZXE6IFJlcXVlc3RFeCwgcmVzOiBFeHByZXNzLlJlc3BvbnNlLCBuZXh0OiBFeHByZXNzLk5leHRGdW5jdGlvbikge1xuICAgIGxldCBzdWNjID0gYXdhaXQgd2ViSGFuZGxlLmdldEVudk1vZHVsZSgpLmJlZm9yZShyZXEsIHJlcyk7XG4gICAgaWYgKCFzdWNjKSB7XG4gICAgICAgIC8vIOWmguaenOWksei0peS6hu+8jOmCo+S5iOebtOaOpei/lOWbnue7k+aenOWlveS6hiDot7Pov4flkI7nu63lh6DkuKrmraXpqqRcbiAgICAgICAgZmluaXNoKHJlcSwgcmVzLCBuZXh0KVxuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgLy8g5byA5aeL6Lev55Sx5qih5Z2X5ZCv5YqoXG4gICAgICAgIGdsb2JhbFN0YXJ0KHdlYkhhbmRsZSwgcmVxLCByZXMsIG5leHQpXG4gICAgfVxufVxuXG4vLyDlubPlj7DlvIDlp4tcbmFzeW5jIGZ1bmN0aW9uIGdsb2JhbFN0YXJ0KHdlYkhhbmRsZTogV2ViSGFuZGxlLCByZXE6IFJlcXVlc3RFeCwgcmVzOiBFeHByZXNzLlJlc3BvbnNlLCBuZXh0OiBFeHByZXNzLk5leHRGdW5jdGlvbikge1xuICAgIGxldCBzdWNjID0gYXdhaXQgd2ViSGFuZGxlLmdldEdsb2JhbE1vZHVsZSgpLmJlZm9yZShyZXEsIHJlcyk7XG4gICAgaWYgKCFzdWNjKSB7XG4gICAgICAgIC8vIOWmguaenOWksei0peS6hu+8jOmCo+S5iOebtOaOpei/lOWbnue7k+aenOWlveS6hiDot7Pov4flkI7nu63lh6DkuKrmraXpqqRcbiAgICAgICAgZmluaXNoKHJlcSwgcmVzLCBuZXh0KVxuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgLy8g5byA5aeL6Lev55Sx5qih5Z2X5ZCv5YqoXG4gICAgICAgIHJvdXRlKHdlYkhhbmRsZSwgcmVxLCByZXMsIG5leHQpXG4gICAgfVxufVxuXG4vLyDmr4/kuKrmqKHlnZfnmoTlkI7nva7osIPnlKhcbmFzeW5jIGZ1bmN0aW9uIHJvdXRlKHdlYkhhbmRsZTogV2ViSGFuZGxlLCByZXE6IFJlcXVlc3RFeCwgcmVzOiBFeHByZXNzLlJlc3BvbnNlLCBuZXh0OiBFeHByZXNzLk5leHRGdW5jdGlvbikge1xuICAgIGxldCBsaXN0ID0gcmVxLnBhdGguc3BsaXQoXCIvXCIpO1xuICAgIGxldCBuYW1lID0gbGlzdC5wb3AoKVxuICAgIC8vIOi/memHjOmcgOimgeWQuOaUtuaOieWkmuS9meeahOepulxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbGlzdC5sZW5ndGg7KSB7XG4gICAgICAgIGlmIChsaXN0W2ldID09IFwiXCIpIHtcbiAgICAgICAgICAgIGxpc3Quc3BsaWNlKGksIDEpXG4gICAgICAgICAgICBjb250aW51ZVxuICAgICAgICB9XG4gICAgICAgIGkrK1xuICAgIH1cblxuICAgIGxldCBtb2QgPSBsaXN0LmpvaW4oXCIvXCIpXG4gICAgbGV0IG1kID0gd2ViSGFuZGxlLmdldE1vZHVsZShtb2QpXG4gICAgLy8g6KGo56S66K+35rGC6L+H56iL5Lit5Ye6546w5oSP5aSW5LqG6Lez6L+Hcm91dGXkuoZcbiAgICBpZiAoYXdhaXQgbWQuYmVmb3JlKHJlcSwgcmVzKSkge1xuICAgICAgICBhd2FpdCBtZC5yb3V0ZShuYW1lIHx8IFwiaW5kZXhcIiwgcmVxLCByZXMsIG5leHQpXG4gICAgICAgIC8vIGFmdGVyIOiwg+eUqOaIkOWKn+eahOWMlui1sOS4i+S4gOS4qua1geeoi1xuICAgICAgICBhd2FpdCBtZC5hZnRlcihyZXEsIHJlcylcbiAgICAgICAgZ2xvYmFsQWZ0ZXIod2ViSGFuZGxlLCByZXEsIHJlcywgbmV4dClcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICAgIGdsb2JhbEFmdGVyKHdlYkhhbmRsZSwgcmVxLCByZXMsIG5leHQpXG4gICAgfVxufVxuXG5hc3luYyBmdW5jdGlvbiBnbG9iYWxBZnRlcih3ZWJIYW5kbGU6IFdlYkhhbmRsZSwgcmVxOiBSZXF1ZXN0RXgsIHJlczogRXhwcmVzcy5SZXNwb25zZSwgbmV4dDogRXhwcmVzcy5OZXh0RnVuY3Rpb24pIHtcbiAgICBhd2FpdCB3ZWJIYW5kbGUuZ2V0R2xvYmFsTW9kdWxlKCkuYWZ0ZXIocmVxLCByZXMpO1xuICAgIC8vIOWmguaenOWksei0peS6hu+8jOmCo+S5iOebtOaOpei/lOWbnue7k+aenOWlveS6hiDot7Pov4flkI7nu63lh6DkuKrmraXpqqRcbiAgICBlbnZBZnRlcih3ZWJIYW5kbGUsIHJlcSwgcmVzLCBuZXh0KVxufVxuXG4vLyDmr4/kuKrmqKHlnZfnmoTlkI7nva7osIPnlKhcbmFzeW5jIGZ1bmN0aW9uIGVudkFmdGVyKHdlYkhhbmRsZTogV2ViSGFuZGxlLCByZXE6IFJlcXVlc3RFeCwgcmVzOiBFeHByZXNzLlJlc3BvbnNlLCBuZXh0OiBFeHByZXNzLk5leHRGdW5jdGlvbikge1xuICAgIGF3YWl0IHdlYkhhbmRsZS5nZXRFbnZNb2R1bGUoKS5hZnRlcihyZXEsIHJlcyk7XG4gICAgLy8g5aaC5p6c5aSx6LSl5LqG77yM6YKj5LmI55u05o6l6L+U5Zue57uT5p6c5aW95LqGIOi3s+i/h+WQjue7reWHoOS4quatpemqpFxuICAgIGZpbmlzaChyZXEsIHJlcywgbmV4dClcbn1cblxuLy8g5qih5Z2X55qE5ZCO572u6LCD55SoXG5mdW5jdGlvbiBmaW5pc2gocmVxOiBSZXF1ZXN0RXgsIHJlczogRXhwcmVzcy5SZXNwb25zZSwgbmV4dDogRXhwcmVzcy5OZXh0RnVuY3Rpb24pIHtcbiAgICBpZiAocmVxLnJlc3BvbnNlRGF0YSA9PSB1bmRlZmluZWQpIHJlcy5zdGF0dXMoNDA0KTtcbiAgICBlbHNlIHJlcy5zdGF0dXMoMjAwKS5zZW5kKHJlcS5yZXNwb25zZURhdGEpXG4gICAgcmVzLmVuZCgpXG59XG5cbiJdfQ==