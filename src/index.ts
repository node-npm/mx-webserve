export { NextFunction, Request, Response } from 'express';
export { http_quest } from "./HttpQuest";
export { IProxyOptions } from "./server";
export { SeParamConfigType, WebHandle } from './webHandle';
import { Request } from "express";
import Joi from 'joi';
import { join } from 'path';
import { start } from "./commonRoute";
import { IPFindOne } from './ip';
import { loadModule } from './loadWebFiles';
import { IProxyOptions, WebRouteUnit } from "./server";
import { SeParamConfigType, WebHandle } from './webHandle';

var instance: WebRouteUnit = new WebRouteUnit();
var webHandle: WebHandle = new WebHandle();


interface SwaggerOptions {
    routePath: string;
    basedir: string;
    ext?: string,
    description?: string;
    title?: string;
    version?: string;
    host?: string;
    basePath?: string;
    produces?: string[];
    schemes?: string[];
}

/**
 * 这里负责http的解析，然后传递给对于的处理方法 
 * 目前比较简陋，后续需要增加一下处理
 * 
 * envBefore -> globalBefore -> before -> route -> after -> globalAfter -> envAfter
 * 
 */
export var WebRouteModule = {
    name: 'WebRouteModule',
    /**
     * 是否开启跨域
     */
    openCross() {
        instance.enable("cross")
    },
    // 获取资源实例
    getApp() {
        return instance.app;
    },
    // 开始静态资源目录
    static(pre: string, root: string) {
        return instance.static(pre, root)
    },

    proxy(pre: string, host: string | ((req: Request) => string), options?: IProxyOptions) {
        return instance.proxy(pre, host, options)
    },
    setproxy(ips: string) {
        return instance.app.set("trust proxy", ips)
    },
    /**
     * 初始化模块
     * @param port 
     */
    async init(port: number, webPath: string, env?: string | (() => string), limit?: string | { [x: string]: string | boolean | undefined; gzip?: boolean; json?: string | undefined; text?: string | undefined; raw?: string | undefined; urlencoded?: string | undefined; } | undefined) {
        // 先初始化模块
        webHandle.init(webPath, env)
        // 这里需要增加一个拉起所有的模块文件的功能
        await instance.init(port, [start.bind(webHandle)], limit)
        loadModule(webPath)
        return true
    },
    ////////////////////////////////////////////////////////////////
    /**
    * 注册模块
    * @param prePath 模块的前缀词 例如 /a/b 的前缀就是 /a 或者/a/
    */
    class(prePath?: string | NodeModule): ClassDecorator {
        return webHandle.class(prePath)
    },

    /**
    * 注册路由
    * @param Name 后缀词 例如 /a/b 的后缀就是 b 或者/b
    */
    route(Name?: string): MethodDecorator {
        return webHandle.route(Name);
    },

    /**
    * 检查参数
    * @param name 参数名字
    * @param sType 参数类型
    * @param [change] 是否强制转换类型 
    */
    paramRequired(name: string, sType: SeParamConfigType, change: boolean): MethodDecorator {
        return webHandle.params(name, sType, true, change);
    },

    /**
   * 检查参数
   * @param name 参数名字
   * @param sType 参数类型
   * @param [change] 是否强制转换类型 
   */
    paramOptional(name: string, sType: SeParamConfigType, change: boolean): MethodDecorator {
        return webHandle.params(name, sType, false, change);
    },

    /**
 * 注册参数检查 支持joi模式
 */
    paramJoi(objSch: Joi.ObjectSchema<any>): MethodDecorator {
        return webHandle.paramJoi(objSch);
    },

    /**
     * 模块方法调用前的调用，主要是给数据处理提供前置准备
     */
    before(): MethodDecorator {
        return webHandle.before();
    },

    /**
     * 模块方法调用后调用，主要是给数据处理提供最后的操作
     */
    after(): MethodDecorator {
        return webHandle.after();
    },
    /////////////////////////////////////////////////
    /**
     * 注册环境处理模块
     * @param evnName 环境名字
     */
    envClass(evnName?: string): ClassDecorator {
        return webHandle.envClass(evnName)
    },

    /**
     * 对应环境下先调用
     */
    envBefore(): MethodDecorator {
        return webHandle.envBefore();
    },
    /**
     * 对应环境下处理后调用
     */
    envAfter(): MethodDecorator {
        return webHandle.envAfter();
    },
    /**
     * 注册环境处理模块
     * @param evnName 环境名字
     */
    globalClass(): ClassDecorator {
        return webHandle.globalClass()
    },
    /**
    * 
    */
    globalBefore(): MethodDecorator {
        return webHandle.globalBefore();
    },
    /**
     * 
     */
    globalAfter(): MethodDecorator {
        return webHandle.globalAfter();
    },

    getHandle() {
        return webHandle;
    },

    //////////////////////////////////////////////////////////////
    /**
     * @description 开启Swagger模块
     * @date 2020-01-16
     * @param {{ basedir: string; routePath: string; description?: string; title?: string; version?: string; host?: string; basePath?: string; produces?: string[]; schemes?: string[]; }} { basedir, jsPath, description = 'This is a sample server', title = 'Swagger', version = '1.0.0', host = 'localhost', basePath = '', produces = [
     *             "application/json"
     *         ], schemes = ['http'] }
     */
    openSwagger(
        { basedir,
            host,
            routePath,
            basePath = '',
            ext = ".js",
            description = 'http api server',
            title = 'dreamduke',
            version = '1.0.0',
            produces = [
                "application/json"
            ], schemes = ['http'] }: SwaggerOptions,
    ) {
        let option = {
            swaggerDefinition: {
                info: {
                    description: description,
                    title: title,
                    version: version
                },
                host: host || IPFindOne("IPv4"),
                basePath: basePath,
                produces: produces,
                schemes: schemes,
                securityDefinitions: {
                    JWT: {
                        type: 'apiKey',
                        in: 'header',
                        name: 'Authorization',
                        description: "",
                    }
                }
            },
            route: {
                url: '/swagger',
                docs: '/swagger.json',    //swagger文件 api
            },
            basedir: basedir || process.cwd(), //app absolute path
            files: [join(routePath || './web', '/**/*' + (ext || ".js"))] //Path to the API handle folder
        }

        try {
            const expressSwagger = require('express-swagger-generator')(instance.app);
            expressSwagger(option)
        }
        catch (e) {
            if (e && (e instanceof Error)) {
                console.log('open openSwagger need install node_module : npm install ' + (e.message.match(/\'.*\'/) || []).join(' ').replace(/\'/g, ''))
            }
        }
    },
}
