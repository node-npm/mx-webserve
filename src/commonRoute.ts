import Express from "express";
import { RequestEx, WebHandle } from "./webHandle";

// 合并参数
export function start(this: WebHandle, req: Express.Request, res: Express.Response, next: Express.NextFunction) {
    // 这里处理一下传递参数的问题统一放在 req.params 上面
    if (typeof req.body == 'string') req.body = { __body: req.body }
    if (typeof req.query == 'string') req.query = { __query: req.query }

    req.params = Object.assign(req.params, req.query || {}, req.body || {});

    // res.setHeader("Content-Type", "text/html; charset=utf-8");

    for (let key in req.params)
        if (req.params[key] == "undefined" || req.params[key] == "null") {
            delete req.params[key];
        }

    envStart(this, req as any, res, next);
}

// 平台开始
async function envStart(webHandle: WebHandle, req: RequestEx, res: Express.Response, next: Express.NextFunction) {
    let succ = await webHandle.getEnvModule().before(req, res);
    if (!succ) {
        // 如果失败了，那么直接返回结果好了 跳过后续几个步骤
        finish(req, res, next)
    }
    else {
        // 开始路由模块启动
        globalStart(webHandle, req, res, next)
    }
}

// 平台开始
async function globalStart(webHandle: WebHandle, req: RequestEx, res: Express.Response, next: Express.NextFunction) {
    let succ = await webHandle.getGlobalModule().before(req, res);
    if (!succ) {
        // 如果失败了，那么直接返回结果好了 跳过后续几个步骤
        finish(req, res, next)
    }
    else {
        // 开始路由模块启动
        route(webHandle, req, res, next)
    }
}

// 每个模块的后置调用
async function route(webHandle: WebHandle, req: RequestEx, res: Express.Response, next: Express.NextFunction) {
    let list = req.path.split("/");
    let name = list.pop()
    // 这里需要吸收掉多余的空
    for (let i = 0; i < list.length;) {
        if (list[i] == "") {
            list.splice(i, 1)
            continue
        }
        i++
    }

    let mod = list.join("/")
    let md = webHandle.getModule(mod)
    // 表示请求过程中出现意外了跳过route了
    if (await md.before(req, res)) {
        await md.route(name || "index", req, res, next)
        // after 调用成功的化走下一个流程
        await md.after(req, res)
        globalAfter(webHandle, req, res, next)
    }
    else {
        globalAfter(webHandle, req, res, next)
    }
}

async function globalAfter(webHandle: WebHandle, req: RequestEx, res: Express.Response, next: Express.NextFunction) {
    await webHandle.getGlobalModule().after(req, res);
    // 如果失败了，那么直接返回结果好了 跳过后续几个步骤
    envAfter(webHandle, req, res, next)
}

// 每个模块的后置调用
async function envAfter(webHandle: WebHandle, req: RequestEx, res: Express.Response, next: Express.NextFunction) {
    await webHandle.getEnvModule().after(req, res);
    // 如果失败了，那么直接返回结果好了 跳过后续几个步骤
    finish(req, res, next)
}

// 模块的后置调用
function finish(req: RequestEx, res: Express.Response, next: Express.NextFunction) {
    if (req.responseData == undefined) res.status(404);
    else res.status(200).send(req.responseData)
    res.end()
}

