"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebRouteModule = exports.WebHandle = exports.http_quest = void 0;
var HttpQuest_1 = require("./HttpQuest");
Object.defineProperty(exports, "http_quest", { enumerable: true, get: function () { return HttpQuest_1.http_quest; } });
var webHandle_1 = require("./webHandle");
Object.defineProperty(exports, "WebHandle", { enumerable: true, get: function () { return webHandle_1.WebHandle; } });
const path_1 = require("path");
const commonRoute_1 = require("./commonRoute");
const ip_1 = require("./ip");
const loadWebFiles_1 = require("./loadWebFiles");
const server_1 = require("./server");
const webHandle_2 = require("./webHandle");
var instance = new server_1.WebRouteUnit();
var webHandle = new webHandle_2.WebHandle();
/**
 * 这里负责http的解析，然后传递给对于的处理方法
 * 目前比较简陋，后续需要增加一下处理
 *
 * envBefore -> globalBefore -> before -> route -> after -> globalAfter -> envAfter
 *
 */
exports.WebRouteModule = {
    name: 'WebRouteModule',
    /**
     * 是否开启跨域
     */
    openCross() {
        instance.enable("cross");
    },
    // 获取资源实例
    getApp() {
        return instance.app;
    },
    // 开始静态资源目录
    static(pre, root) {
        return instance.static(pre, root);
    },
    proxy(pre, host, options) {
        return instance.proxy(pre, host, options);
    },
    setproxy(ips) {
        return instance.app.set("trust proxy", ips);
    },
    /**
     * 初始化模块
     * @param port
     */
    init(port, webPath, env, limit) {
        return __awaiter(this, void 0, void 0, function* () {
            // 先初始化模块
            webHandle.init(webPath, env);
            // 这里需要增加一个拉起所有的模块文件的功能
            yield instance.init(port, [commonRoute_1.start.bind(webHandle)], limit);
            (0, loadWebFiles_1.loadModule)(webPath);
            return true;
        });
    },
    ////////////////////////////////////////////////////////////////
    /**
    * 注册模块
    * @param prePath 模块的前缀词 例如 /a/b 的前缀就是 /a 或者/a/
    */
    class(prePath) {
        return webHandle.class(prePath);
    },
    /**
    * 注册路由
    * @param Name 后缀词 例如 /a/b 的后缀就是 b 或者/b
    */
    route(Name) {
        return webHandle.route(Name);
    },
    /**
    * 检查参数
    * @param name 参数名字
    * @param sType 参数类型
    * @param [change] 是否强制转换类型
    */
    paramRequired(name, sType, change) {
        return webHandle.params(name, sType, true, change);
    },
    /**
   * 检查参数
   * @param name 参数名字
   * @param sType 参数类型
   * @param [change] 是否强制转换类型
   */
    paramOptional(name, sType, change) {
        return webHandle.params(name, sType, false, change);
    },
    /**
 * 注册参数检查 支持joi模式
 */
    paramJoi(objSch) {
        return webHandle.paramJoi(objSch);
    },
    /**
     * 模块方法调用前的调用，主要是给数据处理提供前置准备
     */
    before() {
        return webHandle.before();
    },
    /**
     * 模块方法调用后调用，主要是给数据处理提供最后的操作
     */
    after() {
        return webHandle.after();
    },
    /////////////////////////////////////////////////
    /**
     * 注册环境处理模块
     * @param evnName 环境名字
     */
    envClass(evnName) {
        return webHandle.envClass(evnName);
    },
    /**
     * 对应环境下先调用
     */
    envBefore() {
        return webHandle.envBefore();
    },
    /**
     * 对应环境下处理后调用
     */
    envAfter() {
        return webHandle.envAfter();
    },
    /**
     * 注册环境处理模块
     * @param evnName 环境名字
     */
    globalClass() {
        return webHandle.globalClass();
    },
    /**
    *
    */
    globalBefore() {
        return webHandle.globalBefore();
    },
    /**
     *
     */
    globalAfter() {
        return webHandle.globalAfter();
    },
    getHandle() {
        return webHandle;
    },
    //////////////////////////////////////////////////////////////
    /**
     * @description 开启Swagger模块
     * @date 2020-01-16
     * @param {{ basedir: string; routePath: string; description?: string; title?: string; version?: string; host?: string; basePath?: string; produces?: string[]; schemes?: string[]; }} { basedir, jsPath, description = 'This is a sample server', title = 'Swagger', version = '1.0.0', host = 'localhost', basePath = '', produces = [
     *             "application/json"
     *         ], schemes = ['http'] }
     */
    openSwagger({ basedir, host, routePath, basePath = '', ext = ".js", description = 'http api server', title = 'dreamduke', version = '1.0.0', produces = [
        "application/json"
    ], schemes = ['http'] }) {
        let option = {
            swaggerDefinition: {
                info: {
                    description: description,
                    title: title,
                    version: version
                },
                host: host || (0, ip_1.IPFindOne)("IPv4"),
                basePath: basePath,
                produces: produces,
                schemes: schemes,
                securityDefinitions: {
                    JWT: {
                        type: 'apiKey',
                        in: 'header',
                        name: 'Authorization',
                        description: "",
                    }
                }
            },
            route: {
                url: '/swagger',
                docs: '/swagger.json', //swagger文件 api
            },
            basedir: basedir || process.cwd(),
            files: [(0, path_1.join)(routePath || './web', '/**/*' + (ext || ".js"))] //Path to the API handle folder
        };
        try {
            const expressSwagger = require('express-swagger-generator')(instance.app);
            expressSwagger(option);
        }
        catch (e) {
            if (e && (e instanceof Error)) {
                console.log('open openSwagger need install node_module : npm install ' + (e.message.match(/\'.*\'/) || []).join(' ').replace(/\'/g, ''));
            }
        }
    },
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFDQSx5Q0FBeUM7QUFBaEMsdUdBQUEsVUFBVSxPQUFBO0FBRW5CLHlDQUEyRDtBQUEvQixzR0FBQSxTQUFTLE9BQUE7QUFHckMsK0JBQTRCO0FBQzVCLCtDQUFzQztBQUN0Qyw2QkFBaUM7QUFDakMsaURBQTRDO0FBQzVDLHFDQUF1RDtBQUN2RCwyQ0FBMkQ7QUFFM0QsSUFBSSxRQUFRLEdBQWlCLElBQUkscUJBQVksRUFBRSxDQUFDO0FBQ2hELElBQUksU0FBUyxHQUFjLElBQUkscUJBQVMsRUFBRSxDQUFDO0FBZ0IzQzs7Ozs7O0dBTUc7QUFDUSxRQUFBLGNBQWMsR0FBRztJQUN4QixJQUFJLEVBQUUsZ0JBQWdCO0lBQ3RCOztPQUVHO0lBQ0gsU0FBUztRQUNMLFFBQVEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUE7SUFDNUIsQ0FBQztJQUNELFNBQVM7SUFDVCxNQUFNO1FBQ0YsT0FBTyxRQUFRLENBQUMsR0FBRyxDQUFDO0lBQ3hCLENBQUM7SUFDRCxXQUFXO0lBQ1gsTUFBTSxDQUFDLEdBQVcsRUFBRSxJQUFZO1FBQzVCLE9BQU8sUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUE7SUFDckMsQ0FBQztJQUVELEtBQUssQ0FBQyxHQUFXLEVBQUUsSUFBeUMsRUFBRSxPQUF1QjtRQUNqRixPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQTtJQUM3QyxDQUFDO0lBQ0QsUUFBUSxDQUFDLEdBQVc7UUFDaEIsT0FBTyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUE7SUFDL0MsQ0FBQztJQUNEOzs7T0FHRztJQUNHLElBQUksQ0FBQyxJQUFZLEVBQUUsT0FBZSxFQUFFLEdBQTZCLEVBQUUsS0FBNE07O1lBQ2pSLFNBQVM7WUFDVCxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQTtZQUM1Qix1QkFBdUI7WUFDdkIsTUFBTSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLG1CQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUE7WUFDekQsSUFBQSx5QkFBVSxFQUFDLE9BQU8sQ0FBQyxDQUFBO1lBQ25CLE9BQU8sSUFBSSxDQUFBO1FBQ2YsQ0FBQztLQUFBO0lBQ0QsZ0VBQWdFO0lBQ2hFOzs7TUFHRTtJQUNGLEtBQUssQ0FBQyxPQUE2QjtRQUMvQixPQUFPLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUE7SUFDbkMsQ0FBQztJQUVEOzs7TUFHRTtJQUNGLEtBQUssQ0FBQyxJQUFhO1FBQ2YsT0FBTyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRDs7Ozs7TUFLRTtJQUNGLGFBQWEsQ0FBQyxJQUFZLEVBQUUsS0FBd0IsRUFBRSxNQUFlO1FBQ2pFLE9BQU8sU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQ7Ozs7O0tBS0M7SUFDRCxhQUFhLENBQUMsSUFBWSxFQUFFLEtBQXdCLEVBQUUsTUFBZTtRQUNqRSxPQUFPLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVEOztHQUVEO0lBQ0MsUUFBUSxDQUFDLE1BQTZCO1FBQ2xDLE9BQU8sU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxNQUFNO1FBQ0YsT0FBTyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDOUIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsS0FBSztRQUNELE9BQU8sU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFDRCxpREFBaUQ7SUFDakQ7OztPQUdHO0lBQ0gsUUFBUSxDQUFDLE9BQWdCO1FBQ3JCLE9BQU8sU0FBUyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQTtJQUN0QyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxTQUFTO1FBQ0wsT0FBTyxTQUFTLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUNEOztPQUVHO0lBQ0gsUUFBUTtRQUNKLE9BQU8sU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFDRDs7O09BR0c7SUFDSCxXQUFXO1FBQ1AsT0FBTyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUE7SUFDbEMsQ0FBQztJQUNEOztNQUVFO0lBQ0YsWUFBWTtRQUNSLE9BQU8sU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3BDLENBQUM7SUFDRDs7T0FFRztJQUNILFdBQVc7UUFDUCxPQUFPLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNuQyxDQUFDO0lBRUQsU0FBUztRQUNMLE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7SUFFRCw4REFBOEQ7SUFDOUQ7Ozs7OztPQU1HO0lBQ0gsV0FBVyxDQUNQLEVBQUUsT0FBTyxFQUNMLElBQUksRUFDSixTQUFTLEVBQ1QsUUFBUSxHQUFHLEVBQUUsRUFDYixHQUFHLEdBQUcsS0FBSyxFQUNYLFdBQVcsR0FBRyxpQkFBaUIsRUFDL0IsS0FBSyxHQUFHLFdBQVcsRUFDbkIsT0FBTyxHQUFHLE9BQU8sRUFDakIsUUFBUSxHQUFHO1FBQ1Asa0JBQWtCO0tBQ3JCLEVBQUUsT0FBTyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQWtCO1FBRTNDLElBQUksTUFBTSxHQUFHO1lBQ1QsaUJBQWlCLEVBQUU7Z0JBQ2YsSUFBSSxFQUFFO29CQUNGLFdBQVcsRUFBRSxXQUFXO29CQUN4QixLQUFLLEVBQUUsS0FBSztvQkFDWixPQUFPLEVBQUUsT0FBTztpQkFDbkI7Z0JBQ0QsSUFBSSxFQUFFLElBQUksSUFBSSxJQUFBLGNBQVMsRUFBQyxNQUFNLENBQUM7Z0JBQy9CLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsT0FBTyxFQUFFLE9BQU87Z0JBQ2hCLG1CQUFtQixFQUFFO29CQUNqQixHQUFHLEVBQUU7d0JBQ0QsSUFBSSxFQUFFLFFBQVE7d0JBQ2QsRUFBRSxFQUFFLFFBQVE7d0JBQ1osSUFBSSxFQUFFLGVBQWU7d0JBQ3JCLFdBQVcsRUFBRSxFQUFFO3FCQUNsQjtpQkFDSjthQUNKO1lBQ0QsS0FBSyxFQUFFO2dCQUNILEdBQUcsRUFBRSxVQUFVO2dCQUNmLElBQUksRUFBRSxlQUFlLEVBQUssZUFBZTthQUM1QztZQUNELE9BQU8sRUFBRSxPQUFPLElBQUksT0FBTyxDQUFDLEdBQUcsRUFBRTtZQUNqQyxLQUFLLEVBQUUsQ0FBQyxJQUFBLFdBQUksRUFBQyxTQUFTLElBQUksT0FBTyxFQUFFLE9BQU8sR0FBRyxDQUFDLEdBQUcsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsK0JBQStCO1NBQ2hHLENBQUE7UUFFRCxJQUFJO1lBQ0EsTUFBTSxjQUFjLEdBQUcsT0FBTyxDQUFDLDJCQUEyQixDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzFFLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQTtTQUN6QjtRQUNELE9BQU8sQ0FBQyxFQUFFO1lBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFlBQVksS0FBSyxDQUFDLEVBQUU7Z0JBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMERBQTBELEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO2FBQzNJO1NBQ0o7SUFDTCxDQUFDO0NBQ0osQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCB7IE5leHRGdW5jdGlvbiwgUmVxdWVzdCwgUmVzcG9uc2UgfSBmcm9tICdleHByZXNzJztcclxuZXhwb3J0IHsgaHR0cF9xdWVzdCB9IGZyb20gXCIuL0h0dHBRdWVzdFwiO1xyXG5leHBvcnQgeyBJUHJveHlPcHRpb25zIH0gZnJvbSBcIi4vc2VydmVyXCI7XHJcbmV4cG9ydCB7IFNlUGFyYW1Db25maWdUeXBlLCBXZWJIYW5kbGUgfSBmcm9tICcuL3dlYkhhbmRsZSc7XHJcbmltcG9ydCB7IFJlcXVlc3QgfSBmcm9tIFwiZXhwcmVzc1wiO1xyXG5pbXBvcnQgSm9pIGZyb20gJ2pvaSc7XHJcbmltcG9ydCB7IGpvaW4gfSBmcm9tICdwYXRoJztcclxuaW1wb3J0IHsgc3RhcnQgfSBmcm9tIFwiLi9jb21tb25Sb3V0ZVwiO1xyXG5pbXBvcnQgeyBJUEZpbmRPbmUgfSBmcm9tICcuL2lwJztcclxuaW1wb3J0IHsgbG9hZE1vZHVsZSB9IGZyb20gJy4vbG9hZFdlYkZpbGVzJztcclxuaW1wb3J0IHsgSVByb3h5T3B0aW9ucywgV2ViUm91dGVVbml0IH0gZnJvbSBcIi4vc2VydmVyXCI7XHJcbmltcG9ydCB7IFNlUGFyYW1Db25maWdUeXBlLCBXZWJIYW5kbGUgfSBmcm9tICcuL3dlYkhhbmRsZSc7XHJcblxyXG52YXIgaW5zdGFuY2U6IFdlYlJvdXRlVW5pdCA9IG5ldyBXZWJSb3V0ZVVuaXQoKTtcclxudmFyIHdlYkhhbmRsZTogV2ViSGFuZGxlID0gbmV3IFdlYkhhbmRsZSgpO1xyXG5cclxuXHJcbmludGVyZmFjZSBTd2FnZ2VyT3B0aW9ucyB7XHJcbiAgICByb3V0ZVBhdGg6IHN0cmluZztcclxuICAgIGJhc2VkaXI6IHN0cmluZztcclxuICAgIGV4dD86IHN0cmluZyxcclxuICAgIGRlc2NyaXB0aW9uPzogc3RyaW5nO1xyXG4gICAgdGl0bGU/OiBzdHJpbmc7XHJcbiAgICB2ZXJzaW9uPzogc3RyaW5nO1xyXG4gICAgaG9zdD86IHN0cmluZztcclxuICAgIGJhc2VQYXRoPzogc3RyaW5nO1xyXG4gICAgcHJvZHVjZXM/OiBzdHJpbmdbXTtcclxuICAgIHNjaGVtZXM/OiBzdHJpbmdbXTtcclxufVxyXG5cclxuLyoqXHJcbiAqIOi/memHjOi0n+i0o2h0dHDnmoTop6PmnpDvvIznhLblkI7kvKDpgJLnu5nlr7nkuo7nmoTlpITnkIbmlrnms5UgXHJcbiAqIOebruWJjeavlOi+g+eugOmZi++8jOWQjue7remcgOimgeWinuWKoOS4gOS4i+WkhOeQhlxyXG4gKiBcclxuICogZW52QmVmb3JlIC0+IGdsb2JhbEJlZm9yZSAtPiBiZWZvcmUgLT4gcm91dGUgLT4gYWZ0ZXIgLT4gZ2xvYmFsQWZ0ZXIgLT4gZW52QWZ0ZXJcclxuICogXHJcbiAqL1xyXG5leHBvcnQgdmFyIFdlYlJvdXRlTW9kdWxlID0ge1xyXG4gICAgbmFtZTogJ1dlYlJvdXRlTW9kdWxlJyxcclxuICAgIC8qKlxyXG4gICAgICog5piv5ZCm5byA5ZCv6Leo5Z+fXHJcbiAgICAgKi9cclxuICAgIG9wZW5Dcm9zcygpIHtcclxuICAgICAgICBpbnN0YW5jZS5lbmFibGUoXCJjcm9zc1wiKVxyXG4gICAgfSxcclxuICAgIC8vIOiOt+WPlui1hOa6kOWunuS+i1xyXG4gICAgZ2V0QXBwKCkge1xyXG4gICAgICAgIHJldHVybiBpbnN0YW5jZS5hcHA7XHJcbiAgICB9LFxyXG4gICAgLy8g5byA5aeL6Z2Z5oCB6LWE5rqQ55uu5b2VXHJcbiAgICBzdGF0aWMocHJlOiBzdHJpbmcsIHJvb3Q6IHN0cmluZykge1xyXG4gICAgICAgIHJldHVybiBpbnN0YW5jZS5zdGF0aWMocHJlLCByb290KVxyXG4gICAgfSxcclxuXHJcbiAgICBwcm94eShwcmU6IHN0cmluZywgaG9zdDogc3RyaW5nIHwgKChyZXE6IFJlcXVlc3QpID0+IHN0cmluZyksIG9wdGlvbnM/OiBJUHJveHlPcHRpb25zKSB7XHJcbiAgICAgICAgcmV0dXJuIGluc3RhbmNlLnByb3h5KHByZSwgaG9zdCwgb3B0aW9ucylcclxuICAgIH0sXHJcbiAgICBzZXRwcm94eShpcHM6IHN0cmluZykge1xyXG4gICAgICAgIHJldHVybiBpbnN0YW5jZS5hcHAuc2V0KFwidHJ1c3QgcHJveHlcIiwgaXBzKVxyXG4gICAgfSxcclxuICAgIC8qKlxyXG4gICAgICog5Yid5aeL5YyW5qih5Z2XXHJcbiAgICAgKiBAcGFyYW0gcG9ydCBcclxuICAgICAqL1xyXG4gICAgYXN5bmMgaW5pdChwb3J0OiBudW1iZXIsIHdlYlBhdGg6IHN0cmluZywgZW52Pzogc3RyaW5nIHwgKCgpID0+IHN0cmluZyksIGxpbWl0Pzogc3RyaW5nIHwgeyBbeDogc3RyaW5nXTogc3RyaW5nIHwgYm9vbGVhbiB8IHVuZGVmaW5lZDsgZ3ppcD86IGJvb2xlYW47IGpzb24/OiBzdHJpbmcgfCB1bmRlZmluZWQ7IHRleHQ/OiBzdHJpbmcgfCB1bmRlZmluZWQ7IHJhdz86IHN0cmluZyB8IHVuZGVmaW5lZDsgdXJsZW5jb2RlZD86IHN0cmluZyB8IHVuZGVmaW5lZDsgfSB8IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIC8vIOWFiOWIneWni+WMluaooeWdl1xyXG4gICAgICAgIHdlYkhhbmRsZS5pbml0KHdlYlBhdGgsIGVudilcclxuICAgICAgICAvLyDov5nph4zpnIDopoHlop7liqDkuIDkuKrmi4notbfmiYDmnInnmoTmqKHlnZfmlofku7bnmoTlip/og71cclxuICAgICAgICBhd2FpdCBpbnN0YW5jZS5pbml0KHBvcnQsIFtzdGFydC5iaW5kKHdlYkhhbmRsZSldLCBsaW1pdClcclxuICAgICAgICBsb2FkTW9kdWxlKHdlYlBhdGgpXHJcbiAgICAgICAgcmV0dXJuIHRydWVcclxuICAgIH0sXHJcbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcbiAgICAvKipcclxuICAgICog5rOo5YaM5qih5Z2XXHJcbiAgICAqIEBwYXJhbSBwcmVQYXRoIOaooeWdl+eahOWJjee8gOivjSDkvovlpoIgL2EvYiDnmoTliY3nvIDlsLHmmK8gL2Eg5oiW6ICFL2EvXHJcbiAgICAqL1xyXG4gICAgY2xhc3MocHJlUGF0aD86IHN0cmluZyB8IE5vZGVNb2R1bGUpOiBDbGFzc0RlY29yYXRvciB7XHJcbiAgICAgICAgcmV0dXJuIHdlYkhhbmRsZS5jbGFzcyhwcmVQYXRoKVxyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICog5rOo5YaM6Lev55SxXHJcbiAgICAqIEBwYXJhbSBOYW1lIOWQjue8gOivjSDkvovlpoIgL2EvYiDnmoTlkI7nvIDlsLHmmK8gYiDmiJbogIUvYlxyXG4gICAgKi9cclxuICAgIHJvdXRlKE5hbWU/OiBzdHJpbmcpOiBNZXRob2REZWNvcmF0b3Ige1xyXG4gICAgICAgIHJldHVybiB3ZWJIYW5kbGUucm91dGUoTmFtZSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgKiDmo4Dmn6Xlj4LmlbBcclxuICAgICogQHBhcmFtIG5hbWUg5Y+C5pWw5ZCN5a2XXHJcbiAgICAqIEBwYXJhbSBzVHlwZSDlj4LmlbDnsbvlnotcclxuICAgICogQHBhcmFtIFtjaGFuZ2VdIOaYr+WQpuW8uuWItui9rOaNouexu+WeiyBcclxuICAgICovXHJcbiAgICBwYXJhbVJlcXVpcmVkKG5hbWU6IHN0cmluZywgc1R5cGU6IFNlUGFyYW1Db25maWdUeXBlLCBjaGFuZ2U6IGJvb2xlYW4pOiBNZXRob2REZWNvcmF0b3Ige1xyXG4gICAgICAgIHJldHVybiB3ZWJIYW5kbGUucGFyYW1zKG5hbWUsIHNUeXBlLCB0cnVlLCBjaGFuZ2UpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgKiDmo4Dmn6Xlj4LmlbBcclxuICAgKiBAcGFyYW0gbmFtZSDlj4LmlbDlkI3lrZdcclxuICAgKiBAcGFyYW0gc1R5cGUg5Y+C5pWw57G75Z6LXHJcbiAgICogQHBhcmFtIFtjaGFuZ2VdIOaYr+WQpuW8uuWItui9rOaNouexu+WeiyBcclxuICAgKi9cclxuICAgIHBhcmFtT3B0aW9uYWwobmFtZTogc3RyaW5nLCBzVHlwZTogU2VQYXJhbUNvbmZpZ1R5cGUsIGNoYW5nZTogYm9vbGVhbik6IE1ldGhvZERlY29yYXRvciB7XHJcbiAgICAgICAgcmV0dXJuIHdlYkhhbmRsZS5wYXJhbXMobmFtZSwgc1R5cGUsIGZhbHNlLCBjaGFuZ2UpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICog5rOo5YaM5Y+C5pWw5qOA5p+lIOaUr+aMgWpvaeaooeW8j1xyXG4gKi9cclxuICAgIHBhcmFtSm9pKG9ialNjaDogSm9pLk9iamVjdFNjaGVtYTxhbnk+KTogTWV0aG9kRGVjb3JhdG9yIHtcclxuICAgICAgICByZXR1cm4gd2ViSGFuZGxlLnBhcmFtSm9pKG9ialNjaCk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICog5qih5Z2X5pa55rOV6LCD55So5YmN55qE6LCD55So77yM5Li76KaB5piv57uZ5pWw5o2u5aSE55CG5o+Q5L6b5YmN572u5YeG5aSHXHJcbiAgICAgKi9cclxuICAgIGJlZm9yZSgpOiBNZXRob2REZWNvcmF0b3Ige1xyXG4gICAgICAgIHJldHVybiB3ZWJIYW5kbGUuYmVmb3JlKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICog5qih5Z2X5pa55rOV6LCD55So5ZCO6LCD55So77yM5Li76KaB5piv57uZ5pWw5o2u5aSE55CG5o+Q5L6b5pyA5ZCO55qE5pON5L2cXHJcbiAgICAgKi9cclxuICAgIGFmdGVyKCk6IE1ldGhvZERlY29yYXRvciB7XHJcbiAgICAgICAgcmV0dXJuIHdlYkhhbmRsZS5hZnRlcigpO1xyXG4gICAgfSxcclxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cclxuICAgIC8qKlxyXG4gICAgICog5rOo5YaM546v5aKD5aSE55CG5qih5Z2XXHJcbiAgICAgKiBAcGFyYW0gZXZuTmFtZSDnjq/looPlkI3lrZdcclxuICAgICAqL1xyXG4gICAgZW52Q2xhc3MoZXZuTmFtZT86IHN0cmluZyk6IENsYXNzRGVjb3JhdG9yIHtcclxuICAgICAgICByZXR1cm4gd2ViSGFuZGxlLmVudkNsYXNzKGV2bk5hbWUpXHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICog5a+55bqU546v5aKD5LiL5YWI6LCD55SoXHJcbiAgICAgKi9cclxuICAgIGVudkJlZm9yZSgpOiBNZXRob2REZWNvcmF0b3Ige1xyXG4gICAgICAgIHJldHVybiB3ZWJIYW5kbGUuZW52QmVmb3JlKCk7XHJcbiAgICB9LFxyXG4gICAgLyoqXHJcbiAgICAgKiDlr7nlupTnjq/looPkuIvlpITnkIblkI7osIPnlKhcclxuICAgICAqL1xyXG4gICAgZW52QWZ0ZXIoKTogTWV0aG9kRGVjb3JhdG9yIHtcclxuICAgICAgICByZXR1cm4gd2ViSGFuZGxlLmVudkFmdGVyKCk7XHJcbiAgICB9LFxyXG4gICAgLyoqXHJcbiAgICAgKiDms6jlhoznjq/looPlpITnkIbmqKHlnZdcclxuICAgICAqIEBwYXJhbSBldm5OYW1lIOeOr+Wig+WQjeWtl1xyXG4gICAgICovXHJcbiAgICBnbG9iYWxDbGFzcygpOiBDbGFzc0RlY29yYXRvciB7XHJcbiAgICAgICAgcmV0dXJuIHdlYkhhbmRsZS5nbG9iYWxDbGFzcygpXHJcbiAgICB9LFxyXG4gICAgLyoqXHJcbiAgICAqIFxyXG4gICAgKi9cclxuICAgIGdsb2JhbEJlZm9yZSgpOiBNZXRob2REZWNvcmF0b3Ige1xyXG4gICAgICAgIHJldHVybiB3ZWJIYW5kbGUuZ2xvYmFsQmVmb3JlKCk7XHJcbiAgICB9LFxyXG4gICAgLyoqXHJcbiAgICAgKiBcclxuICAgICAqL1xyXG4gICAgZ2xvYmFsQWZ0ZXIoKTogTWV0aG9kRGVjb3JhdG9yIHtcclxuICAgICAgICByZXR1cm4gd2ViSGFuZGxlLmdsb2JhbEFmdGVyKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldEhhbmRsZSgpIHtcclxuICAgICAgICByZXR1cm4gd2ViSGFuZGxlO1xyXG4gICAgfSxcclxuXHJcbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xyXG4gICAgLyoqXHJcbiAgICAgKiBAZGVzY3JpcHRpb24g5byA5ZCvU3dhZ2dlcuaooeWdl1xyXG4gICAgICogQGRhdGUgMjAyMC0wMS0xNlxyXG4gICAgICogQHBhcmFtIHt7IGJhc2VkaXI6IHN0cmluZzsgcm91dGVQYXRoOiBzdHJpbmc7IGRlc2NyaXB0aW9uPzogc3RyaW5nOyB0aXRsZT86IHN0cmluZzsgdmVyc2lvbj86IHN0cmluZzsgaG9zdD86IHN0cmluZzsgYmFzZVBhdGg/OiBzdHJpbmc7IHByb2R1Y2VzPzogc3RyaW5nW107IHNjaGVtZXM/OiBzdHJpbmdbXTsgfX0geyBiYXNlZGlyLCBqc1BhdGgsIGRlc2NyaXB0aW9uID0gJ1RoaXMgaXMgYSBzYW1wbGUgc2VydmVyJywgdGl0bGUgPSAnU3dhZ2dlcicsIHZlcnNpb24gPSAnMS4wLjAnLCBob3N0ID0gJ2xvY2FsaG9zdCcsIGJhc2VQYXRoID0gJycsIHByb2R1Y2VzID0gW1xyXG4gICAgICogICAgICAgICAgICAgXCJhcHBsaWNhdGlvbi9qc29uXCJcclxuICAgICAqICAgICAgICAgXSwgc2NoZW1lcyA9IFsnaHR0cCddIH1cclxuICAgICAqL1xyXG4gICAgb3BlblN3YWdnZXIoXHJcbiAgICAgICAgeyBiYXNlZGlyLFxyXG4gICAgICAgICAgICBob3N0LFxyXG4gICAgICAgICAgICByb3V0ZVBhdGgsXHJcbiAgICAgICAgICAgIGJhc2VQYXRoID0gJycsXHJcbiAgICAgICAgICAgIGV4dCA9IFwiLmpzXCIsXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uID0gJ2h0dHAgYXBpIHNlcnZlcicsXHJcbiAgICAgICAgICAgIHRpdGxlID0gJ2RyZWFtZHVrZScsXHJcbiAgICAgICAgICAgIHZlcnNpb24gPSAnMS4wLjAnLFxyXG4gICAgICAgICAgICBwcm9kdWNlcyA9IFtcclxuICAgICAgICAgICAgICAgIFwiYXBwbGljYXRpb24vanNvblwiXHJcbiAgICAgICAgICAgIF0sIHNjaGVtZXMgPSBbJ2h0dHAnXSB9OiBTd2FnZ2VyT3B0aW9ucyxcclxuICAgICkge1xyXG4gICAgICAgIGxldCBvcHRpb24gPSB7XHJcbiAgICAgICAgICAgIHN3YWdnZXJEZWZpbml0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICBpbmZvOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGRlc2NyaXB0aW9uLFxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcclxuICAgICAgICAgICAgICAgICAgICB2ZXJzaW9uOiB2ZXJzaW9uXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgaG9zdDogaG9zdCB8fCBJUEZpbmRPbmUoXCJJUHY0XCIpLFxyXG4gICAgICAgICAgICAgICAgYmFzZVBhdGg6IGJhc2VQYXRoLFxyXG4gICAgICAgICAgICAgICAgcHJvZHVjZXM6IHByb2R1Y2VzLFxyXG4gICAgICAgICAgICAgICAgc2NoZW1lczogc2NoZW1lcyxcclxuICAgICAgICAgICAgICAgIHNlY3VyaXR5RGVmaW5pdGlvbnM6IHtcclxuICAgICAgICAgICAgICAgICAgICBKV1Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2FwaUtleScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluOiAnaGVhZGVyJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ0F1dGhvcml6YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJcIixcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHJvdXRlOiB7XHJcbiAgICAgICAgICAgICAgICB1cmw6ICcvc3dhZ2dlcicsXHJcbiAgICAgICAgICAgICAgICBkb2NzOiAnL3N3YWdnZXIuanNvbicsICAgIC8vc3dhZ2dlcuaWh+S7tiBhcGlcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYmFzZWRpcjogYmFzZWRpciB8fCBwcm9jZXNzLmN3ZCgpLCAvL2FwcCBhYnNvbHV0ZSBwYXRoXHJcbiAgICAgICAgICAgIGZpbGVzOiBbam9pbihyb3V0ZVBhdGggfHwgJy4vd2ViJywgJy8qKi8qJyArIChleHQgfHwgXCIuanNcIikpXSAvL1BhdGggdG8gdGhlIEFQSSBoYW5kbGUgZm9sZGVyXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCBleHByZXNzU3dhZ2dlciA9IHJlcXVpcmUoJ2V4cHJlc3Mtc3dhZ2dlci1nZW5lcmF0b3InKShpbnN0YW5jZS5hcHApO1xyXG4gICAgICAgICAgICBleHByZXNzU3dhZ2dlcihvcHRpb24pXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIGlmIChlICYmIChlIGluc3RhbmNlb2YgRXJyb3IpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnb3BlbiBvcGVuU3dhZ2dlciBuZWVkIGluc3RhbGwgbm9kZV9tb2R1bGUgOiBucG0gaW5zdGFsbCAnICsgKGUubWVzc2FnZS5tYXRjaCgvXFwnLipcXCcvKSB8fCBbXSkuam9pbignICcpLnJlcGxhY2UoL1xcJy9nLCAnJykpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG59XHJcbiJdfQ==