import { WebRouteModule, http_quest } from "../src"
import { join } from "path"


describe("web", function () {
    before(function () {
        this.timeout(5000)
        return WebRouteModule.init(8081,join(__dirname, "web"))
    })

    it("test", function () {
        this.timeout(5000)
        return http_quest("get", "http://127.0.0.1:8081/test/test?name=nihao").then(function (v) {
            console.log(v)
        })
    })
    it("test2", function () {
        this.timeout(5000)
        return http_quest("get", "http://127.0.0.1:8081/test/test2?name=nihao").then(function (v) {
            console.log(v)
        })
    })

    after(function (done) {
        setTimeout(function () {
            process.exit(0)
        }, 500)

        done()
    })
})
